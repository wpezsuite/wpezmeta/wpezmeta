<?php

namespace WPezSuite\WPezMeta\Containers\Select;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassSelect extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Escapes\Attr\TraitAttr;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Label\TraitLabel;
    use \WPezSuite\WPezMeta\Core\Traits\Controls\Select\TraitSelect;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;


    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }


    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'], $arr_field['type_args']['options'] ) ) {
            if ( is_array( $arr_field['type_args']['options'] ) ) {
                // to some extent validation. but - for now - why not be extra clean?
                if ( isset( $arr_field['type_args']['options'][ $arr_field['value'] ] ) ) {
                    $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
                    $value = Sanitize::sanitize( 'sanitize_text_field', $value );
                }
            }
        }

        return $value;
    }

}
