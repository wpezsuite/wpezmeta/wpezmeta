<?php

namespace WPezSuite\WPezMeta\Containers\File;

use WPezSuite\WPezMeta\Core\Bases\Containers\File\AbstractClassFile;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

if ( ! class_exists( 'ClassFile' ) ) {
    class ClassFile extends AbstractClassFile {

        use \WPezSuite\WPezMeta\Core\Traits\Scripts\File\TraitFile;
        use \WPezSuite\WPezMeta\Core\Traits\Saves\File\TraitFile;


        /**
         * @return array
         */
        public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

            $arr_defs = [
                'type_args' => [
                    'wp_remote_get_args'          => null,
                    'input_text_url'              => false,
                    // other options?
                    'ext_whitelist'               => false,
                    // array()
                    'ext_blacklist'               => false,
                    // array()
                    'file_source_local'           => true,
                    // https://developer.wordpress.org/reference/functions/wp_upload_bits/
                    'wp_upload_bits_time'         => null,
                    // https://codex.wordpress.org/Function_Reference/wp_remote_get
                    'wp_get_remote_args'          => [],
                    // https://developer.wordpress.org/reference/functions/wp_upload_dir/
                    'wp_upload_dir_time'          => null,
                    'wp_upload_dir_create_dir'    => true,
                    'wp_upload_dir_refresh_cache' => false,
                    // https://codex.wordpress.org/Function_Reference/wp_check_filetype
                    'wp_check_filetype_mimes'     => null,
                    // false = no resize, true = use current WP set, array() = custom sizes
                    'image_sizes'                 => false,
                    // the media_library needs a thumb
                    'thumbnail'                   => true,
                    'media_library'               => true,
                    // 'post_title' or 'field_name' (e.g,, $_POST['field_name']
                    'attachment_post_title'       => 'file_name_clean',
                    'attachment_post_content'     => false,

                    // TODO 'wp_media_multiple' => false,
                ]
            ];

            return array_merge_recursive( $this->baseElementDefaults(), $arr_defs );
        }


        public function registerMeta( $arr_field = [], $wp_object = false, $wp_object = false ) {

            $str_meta_key      = trim( $arr_field['register_meta']['meta_key'] );
            $bool_show_in_rest = trim( $arr_field['register_meta']['args']['show_in_rest'] );

            $arr_ret = [];

            $arr_ret[ $str_meta_key . '_wp_attachment_id' ] = [
                'active'   => true,
                'meta_key' => $str_meta_key . $arr_field['type_args']['wp_attachment_id'],
                'args'     => [
                    'type'         => 'string',
                    'description'  => 'file: so we can also store the _wp_attachment_id',
                    'single'       => true,
                    'show_in_rest' => $bool_show_in_rest
                ]
            ];

            return $arr_ret;
        }


        public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {
            // TODO: Implement sanitize() method.
        }

        protected function elementFileCustom( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

            $str_name = $arr_field['name'];
            $str_type = trim( $arr_field['type'] );

            $arr_ga  = [
                'type'      => 'file',
                'id'        => $str_name,
                'class'     => 'wpezmeta-field-file wpezmeta-field-file-' . $str_type, // TODO - type_args?
                'name'      => $str_name,
                'data-slug' => $str_name
            ];
            $str_ret = '<input ' . $this->global_attrs( $arr_ga ) . '"/>';

            return $str_ret;

        }

    }
}