<?php

namespace WPezSuite\WPezMeta\Containers\Checkbool;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassCheckbool extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Label\TraitLabel;
    use \WPezSuite\WPezMeta\Core\Traits\Controls\Checkbool\TraitCheckbool;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;

    /**
     * you can change the value of checked by setting the field's type_args =>
     * ['checked_value' => ] else the default is 'on'
     *
     * @return array
     */
    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [
            'type_args' => [
                'checked_value' => 'on',
            ]
        ];

        return $arr_defs;
    }

    // TODO ?
    public function escape( $value = '' ) {

        return '';
    }

    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $arr_type_args = $arr_field['type_args'];
            if ( $arr_type_args['checked_value'] == 'on' ) {
                $value = Sanitize::sanitize( 'sanitize_checkbool', $arr_field['value'] );
            } else {
                // if the checked_value has been changed, we need to change the sanitize
                $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
            }
        }

        return $value;
    }
}