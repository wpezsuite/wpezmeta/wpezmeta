<?php

namespace WPezSuite\WPezMeta\Containers\Checkbox;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassCheckbox extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Escapes\Html\TraitHtml;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Radio\TraitRadio;
    use \WPezSuite\WPezMeta\Core\Traits\Controls\Checkbox\TraitCheckBox;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Multi\TraitMulti;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;


    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [
            'type_args' => [

                'fieldset_global_attrs' => [
                    'class' => 'wpezmeta-fieldset'
                ],

                // false triggers the preprocess to use the label
                'legend'                => false,

                'legend_global_attrs' => [
                    'class' => $this->_class_screen_reader_text,
                    //'accesskey' => false
                ],

                'ctrl_wrapper_global_attrs' => [
                    'class' => 'wpezmeta-single-wrapper wpezmeta-checkbox-single-wrapper wpezmeta-display-block'
                ],

            ],
        ];

        return $arr_defs;
    }


    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }


    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            // does sanitizing a serialized array hurt? we should NOT assume we're getting an array
            $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
        }

        return $value;
    }

    /**
     * TODO - move this into a trait? maybe?
     *
     * Kinda a filter/hook for element types that need special treatment (e.g.,
     * checkbox)
     *
     * @param $value
     *
     * @return mixed
     */
    protected function value_return( $value ) {

        $arr_value = [];
        if ( ! empty( $value ) && is_string( $value ) ) {
            $arr_temp = unserialize( $value );
            if ( is_array( $arr_temp ) ) {
                $arr_value = $arr_temp;
            }

        } elseif ( ! empty( $value ) && is_array( $value ) ) {
            $arr_value = $value;
        }

        return $arr_value;
    }

}