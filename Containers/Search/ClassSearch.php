<?php

namespace WPezSuite\WPezMeta\Containers\Search;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassSearch extends AbstractClassInput {

    public function sanitize( $arr_field = [], $str_action = '', $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
        }

        return $value;
    }

}