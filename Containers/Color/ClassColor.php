<?php

namespace WPezSuite\WPezMeta\Containers\Color;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassColor extends AbstractClassInput {

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = '#' . Sanitize::sanitize( 'sanitize_hex', $arr_field['value'] );
        }

        return $value;
    }


    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        // return an [] *always*
        $arr_defs = [
            'global_attrs' => [
                'class' => 'wpezmeta-width-75px wpezmeta-height-75px'
            ],
        ];

        return $arr_defs;
    }

}