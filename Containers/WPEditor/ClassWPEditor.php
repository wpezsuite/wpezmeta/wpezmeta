<?php

namespace WPezSuite\WPezMeta\Containers\WPEditor;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassWPEditor extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\WPEditor\TraitWPEditor;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Label\TraitLabel;
    use \WPezSuite\WPezMeta\Core\Traits\Controls\WPEditor\TraitWPEditor;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;

    /**
     * @var int
     */
    protected $_teeny_mce_buttons_priority = 10;
    // in the event we reuse an instance of this class, make this an array.
    protected $_arr_teeny_mce_buttons = [];

    public function __construct() {

        add_filter( 'teeny_mce_buttons', [ $this, 'teeny_mce_buttons_filter' ], $this->_teeny_mce_buttons_priority, 2 );
    }

    /**
     * We've prepopulated $this->_arr_teeny_mce_buttons[] in the preprocess
     * function
     *
     * @param $arr_buttons
     * @param $editor_id
     *
     * @return mixed
     */
    public function teeny_mce_buttons_filter( $arr_buttons, $editor_id ) {

        if ( isset( $this->_arr_teeny_mce_buttons[ $editor_id ] ) && is_array( $this->_arr_teeny_mce_buttons[ $editor_id ] ) ) {
            return $this->_arr_teeny_mce_buttons[ $editor_id ];
        }

        return $arr_buttons;
    }


    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [
            'type_args' => [
                // false will stop the Add Media button from being rendered
                'media_buttons'     => true,
                'textarea_rows'     => 10,
                'teeny'             => true,
                'teeny_mce_buttons' => [
                    'formatselect',
                    'bold',
                    'italic',
                    'underline',
                    'strikethrough',
                    'bullist',
                    'numlist',
                    // 'blockquote',
                    'alignleft',
                    'aligncenter',
                    'alignright',
                    'link',
                    'unlink',
                    // 'wp_more',
                    // 'undo',
                    // 'redo',
                    'fullscreen'
                ],
            ]
        ];

        return $arr_defs;
    }


    public function escape( $value = '' ) {
    }


    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }


    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'wp_kses_post', $arr_field['value'] );
        }

        return $value;
    }

}