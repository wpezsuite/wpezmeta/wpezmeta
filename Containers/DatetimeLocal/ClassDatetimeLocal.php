<?php

namespace WPezSuite\WPezMeta\Containers\Datetimelocal;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassDatetimeLocal extends AbstractClassInput {

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( ' html5_datetime_local', $arr_field['value'] );
        }

        return $value;
    }

}
