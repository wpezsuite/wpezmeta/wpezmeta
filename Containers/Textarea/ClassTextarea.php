<?php

namespace WPezSuite\WPezMeta\Containers\Textarea;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassTextarea extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Escapes\Textarea\TraitTextarea;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Label\TraitLabel;
    use \WPezSuite\WPezMeta\Core\Traits\Controls\Textarea\TraitTextarea;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;


    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [
            'global_attrs' => [
                'class' => 'wpezmeta-width-auto'
            ],
            'ele_attrs'    => [
                'rows' => '3',
                'cols' => '75'
            ],
        ];

        return $arr_defs;
    }


    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }


    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'sanitize_textarea_field', $arr_field['value'] );
        }

        return $value;
    }

}