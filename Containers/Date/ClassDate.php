<?php

namespace WPezSuite\WPezMeta\Containers\Date;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassDate extends AbstractClassInput {

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'html5_date', $arr_field['value'] );
        }

        return $value;
    }

}
