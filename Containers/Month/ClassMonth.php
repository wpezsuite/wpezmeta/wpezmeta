<?php

namespace WPezSuite\WPezMeta\Containers\Month;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

if ( ! class_exists( 'ClassMonth' ) ) {
    class ClassMonth extends AbstractClassInput {

        public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

            $value = '';
            if ( isset( $arr_field['value'] ) ) {
                $value = Sanitize::sanitize( 'html5_month', $arr_field['value'] );
            }

            return $value;
        }

    }
}