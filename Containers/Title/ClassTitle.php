<?php

namespace WPezSuite\WPezMeta\Containers\Title;

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;

class ClassTitle extends AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
    use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
    use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Title\TraitTitle;


    public function escape( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        return '';
    }

    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }

    public function control( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        return '';
    }

    public function save( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        return '';
    }


    public function getValue( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        return '';
    }

}