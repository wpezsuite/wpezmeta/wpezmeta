<?php

namespace WPezSuite\WPezMeta\Containers\WPMedia;

use WPezSuite\WPezMeta\Core\Bases\Containers\File\AbstractClassFile;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassWPMedia extends AbstractClassFile {

    use \WPezSuite\WPezMeta\Core\Traits\Scripts\WPMedia\TraitWPMedia;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\WPMedia\TraitWPMedia;

    /**
     * @return array
     */
    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [
            'type_args' => [
                'input_text_url'                    => false, // TODO,
                'button_value'                      => 'URL or Add/Upload File',
                'wp_media_button_text'              => 'Add: ',
                'wp_media_button_text_append_label' => true,
                'wp_media_title'                    => 'Select: ',
                'wp_media_title_append_label'       => true,
                'wp_media_type'                     => false, // 'application/pdf',
                // TODO 'wp_media_multiple' => false,
            ]
        ];

        return array_merge_recursive( $this->baseElementDefaults(), $arr_defs );
    }


    public function registerMeta( $arr_field = [], $wp_object = false, $wp_object_id = false ) {

        //    $arr_type_args = $this->type_args($arr_field);

        $str_meta_key      = trim( $arr_field['register_meta']['meta_key'] );
        $bool_show_in_rest = trim( $arr_field['register_meta']['args']['show_in_rest'] );

        $arr_ret = [];

        $arr_ret[ $str_meta_key . '_wp_attachment_id' ] = [
            'active'   => true,
            'meta_key' => $str_meta_key . $arr_field['type_args']['wp_attachment_id'],
            'args'     => [
                'type'         => 'string',
                'description'  => 'file: so we can also store the _wp_attachment_id',
                'single'       => true,
                'show_in_rest' => $bool_show_in_rest
            ]

        ];

        return $arr_ret;
    }

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        // TODO use PHP filter + type_args to validate / sanitize
        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
        }

        return $value;
    }


    // TODO - clean this up
    protected function elementFileCustom( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $arr_type_args = $arr_field['type_args'];

        if ( $arr_type_args['wp_media_button_text_append_label'] === true && $arr_type_args['wp_media_title'] !== false ) {
            $arr_type_args['wp_media_button_text'] = $arr_type_args['wp_media_button_text'] . esc_attr( $arr_field['label'] );
        }

        if ( $arr_type_args['wp_media_title_append_label'] === true && $arr_type_args['wp_media_title'] !== false ) {
            $arr_type_args['wp_media_title'] = $arr_type_args['wp_media_title'] . esc_attr( $arr_field['label'] );
        }

        $arr_data_attrs = [
            'wp_media_button_text' => 'wpmediabuttontext',
            'wp_media_title'       => 'wpmediatitle',
        ];

        $arr_data = [];
        foreach ( $arr_data_attrs as $key => $data_attr ) {

            // TODO just use $data_attr
            if ( $arr_type_args[ $key ] !== false ) {
                $arr_data[] = 'data-' . esc_attr( $data_attr ) . '="' . esc_attr( $arr_type_args[ $key ] ) . '"';
            }
        }
        // data-queryargs="{"type":"application\/pdf"}"
        if ( isset( $arr_type_args['wp_media_type'] )
             && $arr_type_args['wp_media_type'] !== false
        ) {
            $arr_temp   = [ 'type' => $arr_type_args['wp_media_type'] ];
            $arr_data[] = 'data-wpmediatype="' . json_encode( $arr_temp ) . '"';
        }
        $str_name              = esc_attr( $arr_field['name'] );
        $str_button_data_attrs = '';
        if ( ! empty( $arr_data ) ) {
            $str_button_data_attrs = implode( ' ', $arr_data );
        }
        $arr_ga  = [
            'type'      => 'button',
            'id'        => $str_name . '_button',
            'class'     => 'wp-media button',
            'name'      => $str_name . '_button',
            'value'     => $arr_type_args['button_value'],
            'data-slug' => $str_name

        ];
        $str_ret = '<input ' . $this->global_attrs( $arr_ga ) . ' ' . $str_button_data_attrs . '/>';

        $arr_ga = [
            'type'  => 'hidden',
            'id'    => $str_name,
            'class' => 'wp-media button',
            'name'  => $str_name,
        ];

        $value   = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );
        $str_ret .= '<input ' . $this->global_attrs( $arr_ga ) . ' value="' . esc_url( $value['url'] ) . '" />';

        return $str_ret;


    }
}