<?php

namespace WPezSuite\WPezMeta\Containers\Url;

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassUrl extends AbstractClassInput {

    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        // TODO use PHP filter + type_args to validate / sanitize
        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
        }

        return $value;
    }

}