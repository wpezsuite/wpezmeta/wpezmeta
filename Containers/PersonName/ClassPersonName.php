<?php

namespace WPezSuite\WPezMeta\Containers\PersonName;

use WPezSuite\WPezMeta\Core\Bases\Containers\CompositeOne\AbstractClassCompositeOne;
use WPezSuite\WPezCore\Helpers\Sanitize\ClassSanitize as Sanitize;

class ClassPersonName extends AbstractClassCompositeOne {

    use \WPezSuite\WPezMeta\Core\Traits\Preps\Legend\TraitLegend;

    // use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
    // use \WPezSuite\WPezMeta\Core\Traits\Escapes\Attr\TraitAttr;
    // extra
    // use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
    use \WPezSuite\WPezMeta\Core\Traits\Labels\Radio\TraitRadio;

    // sanitize
    // use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
    use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;

    public function elementDefaults( $mix = '', $bool_use_default_value = false ) {

        $arr_defs = [

            'type_args' => [

                'fieldset_global_attrs' => [
                    'class' => 'wpezmeta-fieldset labels-font-weight-400'
                ],

                // false triggers the preprocess to use the label
                'legend'                => false,

                'legend_global_attrs' => [
                    'class' => $this->_class_screen_reader_text,
                    //'accesskey' => false
                ],

                'wpezfields' => [

                    'name_honorific' => [
                        'active'             => true,
                        'name'               => 'honorific',
                        'label'              => 'Honorific',
                        'label_global_attrs' => [
                            'class' => 'wpezmeta-display-block'
                        ],
                        'desc'               => 'TODO',
                        'help'               => 'Notice the use of wrapper_class_special and its WPezMeta CSS helper classes',
                        'type'               => 'select',
                        'global_attrs'       => [
                            'class' => 'wpezmeta-width-75px'
                        ],
                        'default_value'      => 'oth',
                        'type_args'          => [
                            'options' => [
                                'mr'   => 'Mr.',
                                'ms'   => 'Ms.',
                                'miss' => 'Miss',
                                'mrs'  => 'Mrs.',
                                'oth'  => 'Other'
                            ]
                        ],
                        'register_meta'      => true,
                    ],

                    'name_first' => [
                        'active'             => true,
                        'name'               => 'first',
                        'label'              => 'First Name',
                        'label_global_attrs' => [
                            'class' => 'wpezmeta-display-block'
                        ],
                        'desc'               => 'TODO',
                        'help'               => 'Notice the use of wrapper_class_special and its WPezMeta CSS helper classes',
                        'type'               => 'text',
                        'global_attrs'       => [
                            'class' => 'wpezmeta-width-200px'
                        ],
                        'default_value'      => 'xxx',
                        'register_meta'      => true,
                    ],

                    'name_mi' => [
                        'active'             => true,
                        'name'               => 'mi',
                        'label'              => 'M.I.',
                        'label_global_attrs' => [
                            'class' => 'wpezmeta-display-block'
                        ],
                        'desc'               => 'TODO',
                        'help'               => 'Notice the use of wrapper_class_special and its WPezMeta CSS helper classes',
                        'type'               => 'text',
                        'global_attrs'       => [
                            'class' => 'wpezmeta-width-50px'
                        ],
                        'register_meta'      => true,
                    ],

                    'name_last' => [
                        'active'             => true,
                        'name'               => 'last',
                        'label'              => 'Last Name',
                        'label_global_attrs' => [
                            'class' => 'wpezmeta-display-block'
                        ],
                        'desc'               => 'Title helps to define a section',
                        'help'               => 'Notice the use of wrapper_class_special and its WPezMeta CSS helper classes',
                        'type'               => 'text',
                        'global_attrs'       => [
                            'class' => 'wpezmeta-width-200px'
                        ],
                        'register_meta'      => true,
                    ]
                ]
            ]
        ];

        return $arr_defs;
    }


    public function control( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $arr_child_fields = $arr_field['type_args']['wpezfields'];

        $str_ret = '';

        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {
            // TODO - additional checking
            if ( is_array( $arr_child_fields ) ) {

                $str_id_fs = $arr_field['name'] . '_fieldset';
                $str_ret   .= '<fieldset id="' . esc_attr( $str_id_fs ) . '" ' . $this->global_attrs( $arr_field['type_args']['fieldset_global_attrs'] ) . '>';
                $str_ret   .= '<legend ' . $this->global_attrs( $arr_field['type_args']['legend_global_attrs'] ) . '">';
                $str_ret   .= esc_html( $arr_field['type_args']['legend'] );
                $str_ret   .= '</legend>';

                $str_ret .= '<table><tr>';

                // TODO - echo vs not echo. probably best to remove the loop and "hardcode" each individual child. maybe?
                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
                // TODO - hardcode the accepted keys, else you can add fields
                foreach ( $arr_child_fields as $key => $arr_child ) {

                    if ( $arr_child['active'] !== false ) {

                        $str_ret .= '<td>';
                        if ( $bool_echo !== false ) {
                            echo '<td>';
                        }

                        $str_ret .= $arr_child['this_object']->label( $arr_child, $wp_object, $wp_object_id, $bool_use_default_value, $bool_echo );
                        $str_ret .= $arr_child['this_object']->control( $arr_child, $wp_object, $wp_object_id, $bool_use_default_value, $bool_echo ) . ' ';

                        $str_ret .= '</td>';
                        if ( $bool_echo !== false ) {
                            echo '</td>';
                        }
                    }
                }

                $str_ret .= '</tr></table>';
                $str_ret .= '</fieldset>';
                if ( $bool_echo !== false ) {
                    echo '</tr></table>';
                    echo '</fieldset>';
                }
            }
        }

        return $str_ret;
    }

    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        return '';
    }


    public function sanitize( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $value = '';
        if ( isset( $arr_field['value'] ) ) {
            $value = Sanitize::sanitize( 'sanitize_text_field', $arr_field['value'] );
        }

        return $value;
    }
}