<?php
/**
 * Of cource you have hand-craft your own post meta box (and still used
 * WPezFields) but this ez-tizes matters even further.
 */

namespace WPezSuite\WPezMeta\Facades\AddMetaBox;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSuite\WPezMeta\Core\Bases\Facades\Base\AbstractClassBase;

use WPezSuite\WPezMeta\Services\Preprocess\ClassPreprocess;
use WPezSuite\WPezMeta\Services\RegisterMeta\ClassRegisterMeta;
use WPezSuite\WPezMeta\Services\EnqueueScripts\ClassEnqueueScripts;
use WPezSuite\WPezMeta\Services\EnqueueStyles\ClassEnqueueStyles;
use WPezSuite\WPezMeta\Services\Wrapper\ClassWrapper;
use WPezSuite\WPezMeta\Services\Nonce\ClassNonce;
use WPezSuite\WPezMeta\Services\Save\ClassSave;


class ClassAddMetaBox extends AbstractClassBase {

    protected $_ezloader_arr_args_defaults = [
        'active'               => true,
        'prefix'               => '',
        'meta_type'            => 'post',
        'add_meta_box'         => false,
        'wpezfields'           => false,
        'save_post_post_types' => false
    ];

    protected $_add_meta_box_defaults = [
        'id'            => 'wpezmeta-meta-box-id',
        'title'         => 'WPezMeta - Meta_Box Title',
        'callback'      => 'addMetaBoxCallback',
        'screen'        => false,
        'context'       => 'advanced',
        'priority'      => 'default',
        'callback_args' => [],
    ];

    protected $_meta_type_defaults = [
        'comment',
        'post',
        'term',
        'user'
    ];


    protected $_register_meta_return = false;
    protected $_prefix;
    protected $_meta_type;
    protected $_add_meta_box;
    protected $_wp_nonce;
    protected $_wpezfields;
    protected $_save_post_post_types;
    protected $_wrapper              = 'add_form_fields_ver';
    protected $_echo                 = true;

    protected $_newEnqueueScripts = false;
    protected $_newEnqueueStyles  = false;


    function __construct( $arr_args = [] ) {
    }


    public function updateEzLoaderArrArgsDefaults( $arr){

        if (is_array( $arr)){
            $this->_ezloader_arr_args_defaults = array_merge($this->_ezloader_arr_args_defaults, $arr);
        }
    }

    public function updateAddMetaBoxDefaults( $arr){

        if (is_array( $arr)){
            $this->_add_meta_box_defaults = array_merge($this->_add_meta_box_defaults, $arr);
        }
    }

    public function updateMetaTypeDefaults( $arr){

        if (is_array( $arr)){
            $this->_meta_type_defaults = array_merge($this->_meta_type_defaults, $arr);
        }
    }

    /**
     * For debugging purposes
     *
     * @return bool
     */
    public function getRegisterMetaReturn() {

        return $this->_register_meta_return;
    }


    public function setWrapper( $str ) {

        // TODO - validation
        $this->_wrapper = (string)$str;
    }

    public function setEcho( $bool ) {

        // TODO - validation
        $this->_echo = (boolean)$bool;
    }


    /**
     * loads the $arr_args to initialize various settings / args
     *
     * @param array $arr_args
     */
    public function ezLoader( $arr_args = [] ) {

        if ( ! is_array( $arr_args ) ) {
            return false;
        }
        $arr_args = array_merge( $this->_ezloader_arr_args_defaults, $arr_args );

        if ( $arr_args['active'] !== true || ! is_array( $arr_args['add_meta_box'] ) || ! is_array( $arr_args['wpezfields'] ) || ! is_array( $arr_args['save_post_post_types'] ) ) {
            return false;
        }

        // if screen is not set...
        if ( ! isset( $arr_args['add_meta_box']['screen'] ) ) {
            // ...use save_post_post_types
            $arr_args['add_meta_box']['screen'] = $arr_args['save_post_post_types'];
        }

        $this->_meta_type = $arr_args['meta_type'];
        if ( $arr_args['meta_type'] === false || ( ! in_array( $arr_args['meta_type'], $this->_meta_type_defaults ) ) ) {
            $this->_meta_type = 'post';
        }

        $this->_save_post_post_types = $arr_args['save_post_post_types'];
        $this->_prefix               = esc_attr( $arr_args['prefix'] );

        $arr_add_meta_box = array_merge( $this->_add_meta_box_defaults, $arr_args['add_meta_box'] );

        // get these wrong and WP won't catch and re-defaulted, so we do :)
        if ( ! in_array( $arr_add_meta_box['context'], [ 'normal', 'side', 'advanced' ] ) ) {
            $arr_add_meta_box['context'] = 'advanced';
        }

        // get these wrong and WP won't catch and re-defaulted, so we do :)
        if ( ! in_array( $arr_add_meta_box['priority'], [ 'low', 'high', 'default' ] ) ) {
            $arr_add_meta_box['priority'] = 'default';
        }

        $this->_add_meta_box = $arr_add_meta_box;

        // array - wpezfields ftw!!
        $this->_wpezfields = [];
        if ( isset( $arr_args['wpezfields'] ) && is_array( $arr_args['wpezfields'] ) ) {

            $this->_wpezfields = $arr_args['wpezfields'];

            // essentially, nonce defaults
            $this->_wp_nonce = [
                'name'   => 'nonce_' . sanitize_file_name( $this->_add_meta_box['id'] ) . '_name',
                'action' => 'nonce_' . sanitize_file_name( $this->_add_meta_box['id'] ) . '_action'
            ];

            // array - keys: 'action', 'name', 'referer', 'echo'
            if ( isset( $arr_args['wp_nonce'] ) ) {

                if ( is_array( $arr_args['wp_nonce'] ) ) {

                    $this->_wp_nonce = array_merge( $this->_wp_nonce, $arr_args['wp_nonce'] );

                } elseif ( $arr_args['wp_nonce'] === false ) {

                    $this->_wp_nonce = false;
                }
            }

            // TODO - allow nonce to get "turned off


        } else {
            return false;
        }
    }



    public function preprocess() {

        $newPreprocess = new ClassPreprocess();
        $newPreprocess->setRegisterMetaObjectType( $this->_meta_type );
        $this->_wpezfields = $newPreprocess->ezLoader( $this->_wpezfields );

    }

    public function registerMeta() {

        $newRegisterMeta             = new ClassRegisterMeta();
        $this->_register_meta_return = $newRegisterMeta->ezLoader( $this->_wpezfields, $this->_meta_type );

    }

    public function wpRegisterScripts() {

        $this->_newEnqueueScripts = new ClassEnqueueScripts();
        $this->_newEnqueueScripts->ezLoader( $this->_wpezfields );
        $this->_newEnqueueScripts->wpRegisterScripts();
    }


    public function wpEnqueueScripts() {

        if ( $this->_newEnqueueScripts !== false ) {
            $this->_newEnqueueScripts->wpEnqueueScripts();
        }
    }

    public function wpRegisterStyles() {

        $this->_newEnqueueStyles = new EnqueueStyles();
        $this->_newEnqueueStyles->ezLoader( $this->_wpezfields );
        $this->_newEnqueueStyles->wpRegisterStyles();
    }

    public function wpEnqueueStyles() {

        if ( $this->_newEnqueueStyles !== false ) {
            $this->_newEnqueueStyles->wpEnqueueStyles();
        }
    }


    // TODO - allow "manual" builds
    public function addMetaBox( $arg_1, $arg_2 = false ) {

        $post      = '';
        $post_type = ''; // "set" by the action hook
        if ( $arg_2 == false ) {
            $post = $arg_1;
        } else {
            $post_type = $arg_1;
            $post      = $arg_2;
        }

        $arr_amb = $this->_add_meta_box;

        $arr_callback = $arr_amb['callback'];
        if ( ! is_array( $arr_amb['callback'] ) ) {
            $arr_callback = [ $this, $arr_amb['callback'] ];
        }

        // https://developer.wordpress.org/reference/functions/add_meta_box/
        // add_meta_box( string $id, string $title, callable $callback, string|array|WP_Screen $screen = null, string $context = 'advanced', string $priority = 'default', array $callback_args = null )
        add_meta_box(
            $arr_amb['id'],
            $arr_amb['title'],
            $arr_callback,
            $arr_amb['screen'],
            $arr_amb['context'],
            $arr_amb['priority'],
            $arr_amb['callback_args']
        );
    }

    function addMetaBoxCallback( $post, $arr_callback_args = [] ) {


        // $new_Make = new Make();
        // $new_Make->ez_loader( $arr_args );
        // $arr_make = $new_Make->process();

        $str_nonce = '';
        if ( $this->_wp_nonce !== false ) {
            $newNonce = new ClassNonce();
            $newNonce->ezLoader( $this->_wp_nonce );
            $str_nonce = $newNonce->wpNonceField();
        }
        echo $str_nonce;


        $bool_use_default_value = false;
        if ( substr( $post->post_date_gmt, 0, 4 ) == '0000' ) {
            $bool_use_default_value = true;
        }

        $arr_args = [
            'wpezfields'        => $this->_wpezfields,
            'wp_object'         => $post,
            'wp_object_id'      => $post->ID,
            'use_default_value' => $bool_use_default_value,
            'wrapper'           => $this->_wrapper,
            'echo'              => $this->_echo
        ];

        $newWrapper = new ClassWrapper();
        $newWrapper->ezLoader( $arr_args );
        $newWrapper->goWrapper();

        return;
    }


    /**
     * @param        $post_id
     * @param        $post
     * @param string $update
     *
     * @return mixed
     */
    function save( $post_id, $post, $update = '' ) {

        if ( ! in_array( $post->post_type, $this->_save_post_post_types ) ) {
            return $post_id;
        }

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        $post_types = get_post_type_object( $post->post_type );
        if ( ! current_user_can( $post_types->cap->edit_post, $post_id ) ) {
            return $post_id;
        }

        //	$this->save_post_action_complete( $this->_wpezfields, $post_id, $post);

        // if we have a nonce then we need to check it
        if ( $this->_wp_nonce !== false ) {
            if ( ! isset( $_POST[ $this->_wp_nonce['name'] ] ) || ! wp_verify_nonce( $_POST[ $this->_wp_nonce['name'] ], $this->_wp_nonce['action'] ) ) {
                return false;
            }
        }

        $arr_args = [
            'wpezfields'   => $this->_wpezfields,
            'wp_object'    => $post,
            'wp_object_id' => $post_id,
        ];

        $new_Save = new ClassSave();
        $new_Save->ezLoader( $arr_args );
        $new_Save->goSave();

    }


}