<?php
/**
 * Of cource you have hand-craft your own post meta box (and still used WPezFields) but this ez-tizes matters even further.
 */

namespace WPezSuite\WPezMeta\Facades;

// No WP? Die! Now!!
if ( ! defined('ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezSuite\WPezMeta\Core\Bases\Facades\Base;

use WPezSuite\WPezMeta\Services\Preprocess;
use WPezSuite\WPezMeta\Services\RegisterMeta;
use WPezSuite\WPezMeta\Services\EnqueueScripts;
use WPezSuite\WPezMeta\Services\EnqueueStyles;
use WPezSuite\WPezMeta\Services\Nonce;
use WPezSuite\WPezMeta\Services\Save;
use WPezSuite\WPezMeta\Services\Wrapper;

if ( ! class_exists( 'TermMeta' ) ) {
	class TermMeta extends Base{

        protected $_prefix;
        protected $_meta_type = 'term';
        protected $_wp_nonce;
        protected $_wpezfields;
        protected $_wrapper_add = 'add_form_fields';
        protected $_wrapper_edit = 'edit_form_fields';


        protected $_new_EnqueueScripts = false;
		protected $_new_EnqueueStyles = false;


		function __construct( $arr_args = array() ) {}

        public function set_wrapper_add($str){

            // TODO - validation
            $this->_wrapper_add = $str;
        }

        public function set_wrapper_edit($str){

            // TODO - validation
            $this->_wrapper_edit = $str;
        }


		/**
		 * loads the $arr_args to initialize various settings / args
		 *
		 * @param array $arr_args
		 */
		public function ez_loader( $arr_args = array() ) {

		    if ( ! is_array($arr_args) ){
		        return false;
            }
		    $arr_args = array_merge($this->ez_loader_arr_args_defaults(), $arr_args);

		    // required
		    if ( $arr_args['active'] !== true || ! is_array( $arr_args['wpezfields']) || $arr_args['term_box_id'] === false ){
		        return false;
            }

            $this->_term_box_id = $arr_args['term_box_id'];

            // TODO
            $this->_prefix = esc_attr($arr_args['prefix']);

            // array - wpezfields ftw!!
            $this->_wpezfields = [];
            if (isset($arr_args['wpezfields']) && is_array($arr_args['wpezfields'])) {

                $this->_wpezfields = $arr_args['wpezfields'];

                // essentially, nonce defaults
                $this->_wp_nonce = [
                    'name'   => 'nonce_' . sanitize_file_name($this->_term_box_id) . '_name',
                    'action' => 'nonce_' . sanitize_file_name($this->_term_box_id) . '_action'
                ];

                // array - keys: 'action', 'name', 'referer', 'echo'
                if ( isset($arr_args['wp_nonce']) ) {

                    if (is_array($arr_args['wp_nonce'])) {

                        $this->_wp_nonce = array_merge($this->_wp_nonce, $arr_args['wp_nonce']);

                    } elseif ( $arr_args['wp_nonce'] === false ) {

                        // turn off the nonce? be careful!
                        $this->_wp_nonce = false;
                    }
                }

                // TODO - allow nonce to get "turned off


            } else {
                return false;
            }
        }

        protected function ez_loader_arr_args_defaults(){

            $arr_defs = [
                'active' => true,
                'prefix' => '',
                'term_box_id' => false,
                'wpezfields' => false,
                'wp_nonce' => 'true'
            ];

            return $arr_defs;
        }


        public function preprocessor(){

            $new_Preprocessor = new Preprocessor();
            $new_Preprocessor->set_register_meta_object_type($this->_meta_type );
            $this->_wpezfields = $new_Preprocessor->ez_loader( $this->_wpezfields);
        }


        public function instantiate(){

            $new_Instantiate = new Instantiate();
            $this->_wpezfields = $new_Instantiate->ez_loader($this->_wpezfields);
        }

        public function registerMeta(){

            $new_RegisterMeta = new RegisterMeta();
            $new_RegisterMeta->ez_loader($this->_wpezfields, $this->_meta_type);

        }

        public function wp_register_scripts(){

            $this->_new_EnqueueScripts = new EnqueueScripts();
            $this->_new_EnqueueScripts->ez_loader($this->_wpezfields);
            $this->_new_EnqueueScripts->wp_register_scripts();
        }


        public function wp_enqueue_scripts(){

            if (  $this->_new_EnqueueScripts !== false) {
                $this->_new_EnqueueScripts->wp_enqueue_scripts();
            }
        }

        public function wp_register_styles(){

            $this->_new_EnqueueStyles = new EnqueueStyles();
            $this->_new_EnqueueStyles->ez_loader($this->_wpezfields);
            $this->_new_EnqueueStyles->wp_register_styles();
        }

        public function wp_enqueue_styles(){

            if (  $this->_new_EnqueueStyles !== false) {
                $this->_new_EnqueueStyles->wp_enqueue_styles();
            }
        }


        // add form
        public function add_form_fields($taxonomy){

            $this->form_fields($taxonomy, true, $this->_wrapper_add);

        }

        // edit form
        public function edit_form_fields($taxonomy){

            $this->form_fields($taxonomy, false, $this->_wrapper_edit);
        }


        public function form_fields($taxonomy, $bool_use_default_value, $wrapper = ''){

            // $new_Make = new Make();
            // $new_Make->ez_loader( $arr_args );
            // $arr_make = $new_Make->process();

            $str_nonce = '';
            if ( $this->_wp_nonce !== false ) {

                $new_Nonce = new Nonce();
                $new_Nonce->ez_loader($this->_wp_nonce);
                $str_nonce = $new_Nonce->wp_nonce_field();
            }

            echo $str_nonce;

            $wp_object_id = 0;
            if ( $taxonomy instanceof \WP_Term){
                $wp_object_id = $taxonomy->term_id;
            }


            $arr_args = [
                'wpezfields' => $this->_wpezfields,
                'wp_object'  => $taxonomy,
                'wp_object_id' => $wp_object_id,
                'use_default_value' => $bool_use_default_value,
                'wrapper' => $wrapper,
                'echo' => true
            ];

            //  TODO the wrapper is going to vary.
            $new_Wrapper = new Wrapper;
            $new_Wrapper->ez_loader($arr_args);
            $new_Wrapper->render();

            return;

        }

        // aks save
        public function create_edit( $term_id){

            // if we have a nonce then we need to check it
            if ( $this->_wp_nonce !== false ) {
                if ( ! isset( $_POST[ $this->_wp_nonce['name'] ] ) || ! wp_verify_nonce( $_POST[ $this->_wp_nonce['name'] ], $this->_wp_nonce['action'] ) ) {
                    return false;
                }
            }

            $new_Save = new Save();

            $arr_args = [
                'wpezfields' => $this->_wpezfields,
                'wp_object' => get_term($term_id),
                'wp_object_id' => $term_id,
            ];

            $new_Save->ez_loader($arr_args);
            $new_Save->process();

        }


	}
}