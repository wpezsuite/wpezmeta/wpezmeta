//https://scotch.io/tutorials/use-the-html5-file-api-to-work-with-files-locally-in-the-browser
// http://codepen.io/SpencerCooley/pen/JtiFL

jQuery(document).ready(function () {
//check if browser supports file api and filereader features
    if (window.File && window.FileReader && window.FileList && window.Blob) {

        //this is not completely neccessary, just a nice function I found to make the file size format friendlier
        //http://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable
        function humanFileSize(bytes, si) {
            var thresh = si ? 1000 : 1024;
            if (bytes < thresh) return bytes + ' B';
            var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while (bytes >= thresh);
            return bytes.toFixed(1) + ' ' + units[u];
        }

        //watch for change on the
        jQuery(".wpezmeta-field-file").change(function () {

           var theFile = this.files[0];
           var theName = theFile.name;
           var theSlug = jQuery(this).data('slug');

            var reader = new FileReader();
            reader.onload = function (event) {

                var theUrl = event.target.result;

                console.log('name ' + theName);
                console.log('url ' + theUrl);
                console.log('slug ' + theSlug);

                wpezmetaRenderFile( theName, theUrl, theSlug);
                wpezmetaRenderImage( theName, theUrl, theSlug);
                jQuery('#' + theSlug + '_extra').removeClass('display-none');
            }

            reader.readAsDataURL(theFile);
        });


        jQuery(".wpezmeta-field-file-extra :input[type=checkbox]").change(function () {
            jQuery('#' + this.value + '_extra').toggleClass('wpezmeta-file-remove');
        });


        jQuery("#the-video-file-field").change(function () {
            console.log("video file has been chosen");
            //grab the first image in the fileList
            //in this example we are only loading one file.
            console.log(this.files[0].size);
            wpezmetaRenderVideo(this.files[0])

        });

    } else {

        alert('The File APIs are not fully supported in this browser.');

    }
});
