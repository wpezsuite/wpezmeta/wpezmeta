function wpezmetaRenderFile(theName, theUrl, theSlug) {

    console.log('theSlug ' + theSlug);
    var eleDisp = jQuery('#' + theSlug + '_extra .wpezmeta-file-display');
    if (eleDisp !== "undefined") {
        var eleType = eleDisp.prev().is("span");
        if (eleType === false) {
            eleDisp.attr('href', theUrl);
            eleDisp.attr('title', theName);
            eleDisp.text(theName);
        } else {
            eleDisp.text(theName);
        }
    }
}

//this function is called when the input loads an image
function wpezmetaRenderImage(theName, theUrl, theSlug) {

    jQuery('#' + theSlug + '_extra :input[type=checkbox]').attr('checked', false);
    var eleDisp = jQuery('#' + theSlug + '_extra').removeClass('wpezmeta-file-remove').find('.wpezmeta-image-wrap');

    var theImg = eleDisp.find('img');
    if (theImg.attr('src') !== "undefined") {
        theImg.attr('src', theUrl);
        theImg.find('img').attr('title', theName);
        theImg.find('img').attr('alt', theName);
    }
    var theA = eleDisp.find('a');
    if (theA.attr('href') !== "undefined") {
        theA.attr('href', theUrl);
        theA.attr('title', theName);
    }
}

// TODO - this function is called when the input loads a video
function wpezmetaRenderVideo(file) {
    var reader = new FileReader();
    reader.onload = function (event) {
        the_url = event.target.result;
        //of course using a template library like handlebars.js is a better solution than just inserting a string
        jQuery('#data-vid').html("<video width='400' controls><source id='vid-source' src='" + the_url + "' type='video/mp4'></video>");
        jQuery('#name-vid').html(file.name);
        jQuery('#size-vid').html(humanFileSize(file.size, "MB"));
        jQuery('#type-vid').html(file.type);

    };
    //when the file is read it triggers the onload event above.
    reader.readAsDataURL(file);
}