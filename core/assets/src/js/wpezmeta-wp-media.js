// https://mikejolley.com/2012/12/21/using-the-new-wordpress-3-5-media-uploader-in-plugins/

jQuery(document).ready(function($) {
    var file_frame;

    jQuery('.wp-media.button').on('click', function (event) {

        event.preventDefault();

        var wpmediatitle = jQuery(this).data('wpmediatitle');
        var wpmediabuttontext = jQuery(this).data('wpmediabuttontext');
        var wpmediatype = jQuery(this).data('wpmediatype');
        var theSlug = jQuery(this).data('slug');

        console.log('theSlug ' + theSlug);
        var multi = false;

        // Create the media frame.
        file_frame = wp.media.frames.file_frame = wp.media({
            title: wpmediatitle  || '',
            library : wpmediatype || {},
            button: {
                text: wpmediabuttontext || ''
            },
            multiple: multi
        });

        // When an image is selected, run a callback.
        file_frame.on('select', function () {

            if ( multi !== true){
                attachment = file_frame.state().get('selection').first().toJSON();
                theUrl = attachment.url;
                theName = attachment.filename;
                jQuery('#' + theSlug + '_file_url_id').val(theUrl);
                wpezmetaRenderFile( theName, theUrl, theSlug);
                wpezmetaRenderImage( theName, theUrl, theSlug);
                jQuery('#' + theSlug + '_id').val(attachment.id);
                jQuery('#' + theSlug ).val(theUrl);
                jQuery('#' + theSlug + '_extra').removeClass('display-none');
            } else {
                // multi === TODO
            }
        });
        // Finally, open the modal
        file_frame.open();
    });
});