<?php
/*
 * "The ezWay" to make, display and save: tax terms and post meta fields.
 *
 * IMPORTANT: To use this in your own plugin or theme you MUST (please) change the namespace!!!
 */

namespace WPezSuite\WPezMeta\Core\Sanitize;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassSanitize {

    protected static $_default_method = 'sanitize_text_field';


    public static function method_exits ( $str_method ) {

        if ( method_exists( __CLASS__, $str_method ) ) {
            return true;
        }

        return false;
    }


    public static function sanitize ( $str_method, $value ) {

        // maybe we want to live dangerously and not sanitize
        if ( $str_method === false ) {
            return $value;
        }
        if ( method_exists( __CLASS__, $str_method ) ) {
            return self::$str_method( $value );
        }
        $str_method = self::$_default_method;

        return $str_method( $value );

    }

    // https://codex.wordpress.org/Function_Reference/sanitize_email
    protected static function sanitize_email ( $value ) {

        return sanitize_email( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_file_name
    protected static function sanitize_file_name ( $value ) {

        return sanitize_file_name( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_html_class
    // "Sanitizes a html classname to ensure it only contains valid characters."
    protected static function sanitize_html_class ( $value ) {

        return sanitize_html_class( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_key
    protected static function sanitize_key ( $value ) {

        return sanitize_key( $value );
    }

    // sanitize_meta
    // https://codex.wordpress.org/Function_Reference/sanitize_meta
    // not available here but listed for completeness


    // https://codex.wordpress.org/Function_Reference/sanitize_mime_type
    protected static function sanitize_mime_type ( $value ) {

        return sanitize_mime_type( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_option
    protected static function sanitize_option ( $value ) {

        return sanitize_option( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_sql_orderby
    protected static function sanitize_sql_orderby ( $value ) {

        return sanitize_sql_orderby( $value );
    }

    // https://developer.wordpress.org/reference/functions/sanitize_text_field/
    protected static function sanitize_text_field ( $value ) {

        return sanitize_text_field( $value );
    }


    // https://developer.wordpress.org/reference/functions/sanitize_textarea_field/
    protected static function sanitize_textarea_field ( $value ) {

        return sanitize_textarea_field( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_title
    protected static function sanitize_title ( $value ) {

        return sanitize_title( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_title_for_query
    protected static function sanitize_title_for_query ( $value ) {

        return sanitize_title_for_query( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes
    protected static function sanitize_title_with_dashes ( $value ) {

        return sanitize_title_with_dashes( $value );
    }

    // https://codex.wordpress.org/Function_Reference/sanitize_user
    protected static function sanitize_user ( $value ) {

        return sanitize_user( $value );
    }

    // https://codex.wordpress.org/wp_kses_post
    protected static function wp_kses_post ( $value ) {

        return wp_kses_post( $value );
    }

    // https://codex.wordpress.org/Function_Reference/tag_escape
    // "Removes all characters other than a-zA-Z0-9_:, the set of valid HTML
    // tag characters. Transforms letters to lowercase."
    protected static function esc_tag ( $value ) {

        return tag_escape( $value );
    }

    // https://developer.wordpress.org/reference/functions/tag_escape/
    // Escape an HTML tag name.
    protected static function tag_escape ( $value ) {

        return tag_escape( $value );
    }


    // https://codex.wordpress.org/Function_Reference/esc_textarea
    // Encodes text for use inside a <textarea> element.
    protected static function esc_textarea ( $value ) {

        return esc_textarea( $value );
    }

    // https://developer.wordpress.org/reference/functions/esc_url/
    protected static function esc_url ( $value ) {

        return esc_url( $value );
    }


    // https://codex.wordpress.org/Function_Reference/wp_strip_all_tagss
    // Properly strip all HTML tags including script and style.
    protected static function wp_strip_all_tags ( $value ) {

        $bool_remove_breaks = false;

        return wp_strip_all_tags( $value, $bool_remove_breaks );
    }


    // https://codex.wordpress.org/Function_Reference/wp_strip_all_tagss
    // Properly strip all HTML tags including script and style.
    protected static function wp_strip_all_tags_remove_breaks ( $value ) {

        $bool_remove_breaks = true;

        return wp_strip_all_tags( $value, $bool_remove_breaks );
    }

    // https://codex.wordpress.org/Function_Reference/wptexturize
    //
    // "This returns given text with transformations of quotes to smart
    // quotes, apostrophes, dashes, ellipses, the trademark symbol, and
    // the multiplication symbol."
    protected static function wptexturize ( $value ) {

        return wptexturize( $value );
    }

    // http://themehybrid.com/weblog/introduction-to-wordpress-term-meta
    protected static function sanitize_hex ( $value ) {

        $value = ltrim( $value, '#' );

        return preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $value ) ? $value : '';
    }


    // checkbool is a special ez form element for yes (checked) or no (unchecked)
    protected static function sanitize_checkbool ( $value ) {

        if ( ! empty( $value ) ) {
            return 'on';
        }

        return '';
    }


    /**
     * sanitize via validation (format: 2017-09-23T23:01)
     *
     * @param $value
     *
     * @return string
     */
    protected static function html5_datetime_local ( $value ) {

        $date      = \DateTime::createFromFormat( 'Y-m-d H:i', str_replace( 'T', ' ', $value ) );
        $new_value = $date->format( 'Y-m-d' ) . 'T' . $date->format( 'H:i' );
        if ( $value === $new_value ) {
            return $value;
        }

        return '';
    }


    /**
     * sanitize via validation (format: 2017-09-30)
     *
     * @param $value
     *
     * @return string
     */
    protected static function html5_date ( $value ) {

        $new_value = '';
        $date      = \DateTime::createFromFormat( 'Y-m-d', $value );
        if ( $date !== false ) {
            $new_value = $date->format( 'Y-m-d' );
        }

        if ( $value === $new_value ) {
            return $value;
        }

        return '';
    }

    /**
     * sanitize via validation (format: 2017-09)
     *
     * @param $value
     *
     * @return string
     */
    protected static function html5_month ( $value ) {

        $new_value = '';
        $date      = \DateTime::createFromFormat( 'Y-m-d', $value . '-01' );
        if ( $date !== false ) {
            $new_value = $date->format( 'Y-m-d' );
        }

        if ( $value . '-01' === $new_value ) {
            return $value;
        }

        return '';
    }

    /**
     * sanitize via validation (format: 2017-W38)
     *
     * @param $value
     *
     * @return string
     */
    protected static function html5_week ( $value ) {

        $yyyy = substr( $value, 0, 4 );
        $wk   = substr( $value, 6, 2 );

        $new_value = '';
        $date      = new \DateTime();
        if ( $date !== false ) {
            $date->setISODate( $yyyy, $wk );
            $new_value = $date->format( 'Y' ) . '-W' . $date->format( 'W' );
        }

        if ( $value === $new_value ) {
            return $value;
        }

        return '';
    }

    /**
     * sanitize via validation (format: 00:59 - 24 hr clock)
     *
     * @param $value
     *
     * @return string
     */
    protected static function html5_time ( $value ) {

        $new_value = '';
        $date      = \DateTime::createFromFormat( 'H:i', $value );
        if ( $date !== false ) {
            $new_value = $date->format( 'H:i' );
        }

        if ( $value === $new_value ) {
            return $value;
        }

        return '';
    }

    protected static function sanitize_integer ( $value ) {

        $new_value = (integer)$value;

        if ( $value === $new_value ) {
            return $value;
        }

        return '';
    }

}
