<?php

namespace WPezSuite\WPezMeta\Core\Traits\Enqueues\WPezMeta;

trait TraitWPezMeta {

    // TODO add set'ers
    protected $_wpezmeta_css_deps = [];
    protected $_wpezmeta_css_ver  = '0.0.2-a';


    public function wpRegisterStylesWPezMeta() {

        $file   = wp_normalize_path( __FILE__ );
        $needle = '/WPezMeta/Core';
        $start  = 0;
        $length = strrpos( $file, $needle ) + strlen( $needle );
        $url    = substr( $file, $start, $length );
        $url    = str_replace( get_home_path(), get_home_url() . '/', $url );

        wp_register_style(
            'wpezmeta-admin',
            $url . '/assets/src/css/wpezmeta.css',
            $this->_wpezmeta_css_deps,
            $this->_wpezmeta_css_ver,
            'all'
        );
    }


    public function wpEnqueueStylesWPezMeta() {

        wp_enqueue_style( 'wpezmeta-admin' );
    }

}