<?php

// https://hyperform.js.org/
// https://github.com/hyperform/hyperform

namespace WPezSuite\WPezMeta\Core\Traits\Enqueues\Hyperform;

trait TraitHyperform {


    protected $_hyperform_ver    = '0.9.9';
    protected $_hyperform_config = "hyperform(window, {revalidate: 'onsubmit', validEvent: false,});";

    // https://hyperform.js.org/
    public function wpRegisterScriptsHyperform() {

        wp_register_script(
            'hyperform_js',
            'https://cdnjs.cloudflare.com/ajax/libs/hyperform/' . $this->_hyperform_ver . '/hyperform.min.js',
            [],
            $this->_hyperform_ver,
            true
        );
    }

    public function wpEnqueueScriptsHyperform() {

        wp_enqueue_script( 'hyperform_js' );
    }

    // https://hyperform.js.org/docs/usage.html
    public function setHyperformConfig( $str ) {

        if ( is_string( $str ) ) {
            $this->_hyperform_config = $str;
        }
    }


    public function wpAddInlineScriptHyperform() {

        wp_add_inline_script( 'hyperform_js', $this->_hyperform_config );
    }


    public function wpRegisterStylesHyperform() {

        wp_register_style(
            'hyperform_css',
            'https://cdnjs.cloudflare.com/ajax/libs/hyperform/' . $this->_hyperform_ver . '/hyperform.css',
            [],
            $this->_hyperform_ver,
            'all'
        );
    }


    public function wpEnqueueStylesHyperform() {

        wp_enqueue_style( 'hyperform_css' );
    }


}
