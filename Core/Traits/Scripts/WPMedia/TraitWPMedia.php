<?php

namespace WPezSuite\WPezMeta\Core\Traits\Scripts\WPMedia;

trait TraitWPMedia {


    public function scripts ( $arr_field = [], $wp_object_id = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $ver = '0.0.1';

        $file   = wp_normalize_path( __FILE__ );
        $needle = '/WPezMeta/';
        $start  = 0;
        $length = strrpos( $file, $needle ) + strlen( $needle );
        $url    = substr( $file, $start, $length );
        $url    = str_replace( get_home_path(), get_home_url() . '/', $url );

        $arr_scripts = [

            'wpezmeta_file_wp_media' => [
                'active'    => true,
                'handle'    => 'wpezmeta_file_wp_media',
                'src'       => $url . 'core/assets/src/js/wpezmeta-file-wp-media.js',
                'deps'      => [ 'jquery' ],
                'ver'       => $ver, //'0.1.1a',
                'in_footer' => false
            ],

            'wpezmeta_wp_media' => [
                'active'    => true,
                'handle'    => 'wpezmeta_wp_media',
                'src'       => $url . 'core/assets/src/js/wpezmeta-wp-media.js',
                'deps'      => [ 'jquery', 'wpezmeta_file_wp_media' ],
                'ver'       => $ver, //'0.1.1a',
                'in_footer' => false
            ],
        ];

        // print_r($arr_scripts);
        // die('x');

        return $arr_scripts;
    }

}