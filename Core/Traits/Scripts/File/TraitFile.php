<?php

namespace WPezSuite\WPezMeta\Core\Traits\Scripts\File;

trait TraitFile {


    public function scripts( $arr_field = [], $wp_object_id = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $ver = '0.0.1';

        $file   = wp_normalize_path( __FILE__ );
        $needle = '/WPezMeta/';
        $start  = 0;
        $length = strrpos( $file, $needle ) + strlen( $needle );
        $url    = substr( $file, $start, $length );
        $url    = str_replace( get_home_path(), get_home_url() . '/', $url );

        $arr_scripts = [

            'wpezmeta_file_wp_media' => [
                'active'    => true,
                'handle'    => 'wpezmeta_file_wp_media',
                'src'       => $url . 'core/assets/src/js/wpezmeta-file-wp-media.js',
                'deps'      => [ 'jquery' ],
                'ver'       => $ver,
                'in_footer' => false
            ],

            'wpezmeta_file_wp' => [
                'active'    => true, // << dont need this for wp-media TODO-x
                'handle'    => 'wpezmeta_file_wp',
                'src'       => $url . 'core/assets/src/js/wpezmeta-file-wp.js',
                'deps'      => [ 'jquery' ],
                'ver'       => $ver,
                'in_footer' => false
            ],

            'wpezmeta_html5_input_file' => [
                'active'    => true, // >> dont need this for wp-media TODO-x
                'handle'    => 'wpezmeta_html5_input_file',
                'src'       => $url . 'core/assets/src/js/wpezmeta-html5-input-file.js',
                'deps'      => [ 'jquery', 'wpezmeta_file_wp_media' ],
                'ver'       => $ver,
                'in_footer' => false
            ],

        ];

        return $arr_scripts;
    }
}