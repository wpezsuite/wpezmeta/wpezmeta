<?php

namespace WPezSuite\WPezMeta\Core\Traits\Escapes\Textarea;


trait TraitTextarea {

    public function escape ( $mix_val = '' ) {

        return esc_textarea( $mix_val );
    }
}