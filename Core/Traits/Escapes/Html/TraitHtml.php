<?php

namespace WPezSuite\WPezMeta\Core\Traits\Escapes\Html;


trait TraitHtml {

    public function escape ( $value = '' ) {

        return esc_html( $value );
    }
}