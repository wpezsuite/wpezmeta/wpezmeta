<?php

namespace WPezSuite\WPezMeta\Core\Traits\Escapes\UrlRaw;


trait TraitUrlRaw {

    public function escape ( $mix_val = '' ) {

        return esc_url_raw( $mix_val );
    }
}