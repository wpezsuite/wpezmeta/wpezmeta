<?php

namespace WPezSuite\WPezMeta\Core\Traits\Escapes\Url;


trait TraitUrl {

    public function escape ( $mix_val = '' ) {

        return esc_url( $mix_val );
    }
}