<?php

namespace WPezSuite\WPezMeta\Core\Traits\Escapes\Attr;


trait TraitAttr {

    public function escape ( $mix_val = '' ) {

        return esc_attr( $mix_val );
    }
}