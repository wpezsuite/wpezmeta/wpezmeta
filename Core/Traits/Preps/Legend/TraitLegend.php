<?php

namespace WPezSuite\WPezMeta\Core\Traits\Preps\Legend;


trait TraitLegend
{

    public function prep($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false) {

        if ( ! in_array('prep', $arr_field['active_blacklist']) ) {
            if ( isset($arr_field['type_args']['legend']) && $arr_field['type_args']['legend'] === false) {
                $arr_field['type_args']['legend'] = $arr_field['label'];
            }
        }
        return $arr_field;
    }
}