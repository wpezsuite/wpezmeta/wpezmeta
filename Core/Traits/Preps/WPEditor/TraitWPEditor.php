<?php

namespace WPezSuite\WPezMeta\Core\Traits\Preps\WPEditor;


trait TraitWPEditor {

    /**
     * Long to short, this is how we set the mce buttons on an editor by editor
     * basis. There's a filter in the main class
     *
     * @param array $arr_field
     * @param bool  $wp_object
     * @param bool  $wp_object_id
     *
     * @return array
     */
    public function prep( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        if ( ! in_array( 'prep', $arr_field['active_blacklist'] ) ) {

            $str_id = $arr_field['name'];
            if ( isset( $arr_field['type_args']['teeny_mce_buttons'] ) && is_array( $arr_field['type_args']['teeny_mce_buttons'] ) && ! isset( $this->_arr_teeny_mce_buttons[ $str_id ] ) ) {

                if ( isset( $arr_field['type_args']['teeny'] ) && $arr_field['type_args']['teeny'] === false ) {
                    return $arr_field;
                }
                // if there's a teeny_mce_buttons[] we'll set teeny to true - because it needs to be - unless it was already false
                $arr_field['type_args']['teeny'] = true;

                $this->_arr_teeny_mce_buttons[ $str_id ] = $arr_field['type_args']['teeny_mce_buttons'];
            }
        }

        return $arr_field;
    }
}