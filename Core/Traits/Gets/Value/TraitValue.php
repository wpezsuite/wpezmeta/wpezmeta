<?php

namespace WPezSuite\WPezMeta\Core\Traits\Gets\Value;


trait TraitValue {

    use \WPezSuite\WPezMeta\Core\Traits\Gets\Shared\TraitShared;

    public function getValue( $arr_field = [], $wp_object = false, $wp_object_id = false, $use_default_value = false ) {

        $bool_single = true;
        if ( isset( $arr_field['register_meta']['args']['single'] ) ) {
            $bool_single = (boolean)$arr_field['register_meta']['args']['single'];
        }
        $arr_field['register_meta']['args']['single'] = $bool_single;

        if ( ! isset( $arr_field['register_meta']['meta_key'] ) ) {
            return '';
        }
        $str_meta_key = $arr_field['register_meta']['meta_key'];

        if ( ! isset( $arr_field['register_meta']['object_type'] ) ) {
            return '';
        }
        $str_meta_type = $arr_field['register_meta']['object_type'];

        return $this->shared( $arr_field, $wp_object, $wp_object_id, $use_default_value, $str_meta_type, $str_meta_key, $bool_single );

    }

}