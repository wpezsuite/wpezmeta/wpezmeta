<?php

namespace WPezSuite\WPezMeta\Core\Traits\Gets\File;


trait TraitFile {

    use \WPezSuite\WPezMeta\Core\Traits\Gets\Shared\TraitShared;

    public function getValue( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $bool_single = true;
        if ( isset( $arr_field['register_meta']['args']['single'] ) ) {
            $bool_single = (boolean)$arr_field['register_meta']['args']['single'];
        }
        $arr_field['register_meta']['args']['single'] = $bool_single;

        if ( ! isset( $arr_field['register_meta']['meta_key'] ) ) {
            return '';
        }
        $str_meta_key = $arr_field['register_meta']['meta_key'];

        if ( ! isset( $arr_field['register_meta']['object_type'] ) ) {
            return '';
        }
        $str_meta_type = $arr_field['register_meta']['object_type'];

        // file + wp_media is a special case.
        //    if ( $arr_args['type'] == 'file' || $arr_args['type'] == 'wp_media' ) {

        $value       = [];
        $value['id'] = '1';

        $value['url'] = $this->shared( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value, $str_meta_type, $str_meta_key, $bool_single );

        // are we keeping track of the attachment_id or not?
        if ( ! isset( $arr_field['type_args']['media_library'] ) || ( isset( $arr_field['type_args']['media_library'] ) && $arr_field['type_args']['media_library'] !== false ) ) {
            {
                // TODO make the key a default value that can also be set via type_args
                $str_meta_key_id = $str_meta_key . '_id';
                $value['id']     = $this->shared( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value, $str_meta_type, $str_meta_key_id, $bool_single );

            }
            return $value;
            //   }

        }

        return $value;

    }
}