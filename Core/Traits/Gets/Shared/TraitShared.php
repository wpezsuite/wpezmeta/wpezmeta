<?php
namespace WPezSuite\WPezMeta\Core\Traits\Gets\Shared;


trait TraitShared
{


    protected function shared($arr_field = [], $wp_object, $wp_object_id, $bool_use_default_value = false, $str_meta_type = '', $str_meta_key, $bool_single)
    {

        if ( $bool_use_default_value !== false ) {

            if ( isset($arr_field['default_value'])) {
                return $arr_field['default_value'];
            }
            return '';
        }

        $value = get_metadata($str_meta_type, $wp_object_id, $str_meta_key, $bool_single);

        // some elements types (e.g., checkbox) require additional special processing
        return $this->value_return($value);
    }


    /**
     * Kinda a filter/hook for element types that need special treatment (e.g., checkbox)
     * @param $value
     *
     * @return mixed
     */
    protected function value_return($value){

        return $value;
    }

}