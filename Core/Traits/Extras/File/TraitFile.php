<?php

namespace WPezSuite\WPezMeta\Core\Traits\Extras\File;

trait TraitFile {

    public function extra( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'extra', $arr_field['active_blacklist'] ) ) {
            $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

            $str_type = trim( $arr_field['type'] );

            $arr_type_args = $arr_field['type_args'];
            $arr_extra     = $arr_type_args['extra'];

            $str_disp_none = ' display-none';
            if ( isset( $value['url'] ) && ! empty( $value['url'] ) && $value['url'] == esc_url( $value['url'] ) ) {
                $str_disp_none = '';
            }

            $str_ret .= '<div class="wpezmeta-field-extra wpezmeta-field-file-extra'
                        . $str_disp_none . '" id="' . esc_attr( $arr_field['name'] )
                        . '_extra">';

            // TODO - better check
            if ( $arr_type_args['file_active'] ) {

                $str_targ = $this->getTarget( $arr_extra, 'file_view_target' );

                $str_click_open  = '<span class="wpezmeta-file-display">';
                $str_click_close = '</span>';
                if ( $arr_type_args['file_view_active'] === true ) {
                    $str_click_open  = '<a href="' . esc_url( $value['url'] )
                                       . '" class="wpezmeta-file-display" title="'
                                       . esc_url( $value['url'] ) . '"' . $str_targ . '>';
                    $str_click_close = '</a>';
                }

                $str_ret        .= '<span class="wpezmeta-file-wrap">';
                $str_ret        .= $str_click_open;
                $arr_wp_gud     = wp_get_upload_dir();
                $str_value_name = str_replace( $arr_wp_gud['baseurl'], '',
                    $value['url'] );

                $str_ret .= esc_url( $str_value_name );
                $str_ret .= $str_click_close;
                if ( $arr_type_args['file_view_msg'] !== false ) {
                    $str_ret .= '<p class="wpezmeta-desc">'
                                . esc_html( $arr_type_args['file_view_msg'] ) . '</p>';
                }
                $str_ret .= '</span>';
            }


            if ( $arr_type_args['img_active'] ) {

                $str_targ = $this->getTarget( $arr_type_args, 'img_view_target' );

                $str_click_open  = '<span ' . $this->global_attrs( $arr_extra['file_display_global_attrs'] ) . '>';
                $str_click_close = '</span>';
                if ( $arr_type_args['img_view_active'] === true ) {
                    $str_click_open  = '<a href="' . esc_url( $value['url'] ) . '"'
                                       . ' class="' . esc_attr( $arr_extra['image_display_class'] ) . '"'
                                       . ' title="' . esc_url( $value['url'] ) . '"' . $str_targ . '>';
                    $str_click_close = '</a>';
                }

                $str_ret .= '<span ' . $this->global_attrs( $arr_extra['image_wrap_global_attrs'] ) . '>';
                $str_ret .= $str_click_open;
                //	$arr_wp_gud = wp_get_upload_dir();
                //	$str_value = str_replace($arr_wp_gud['baseurl'] , '', $value);

                // TODO - what if file ext is NOT an img???
                // https://en.support.wordpress.com/accepted-filetypes/
                // mainly an issue for the .js?

                $str_file_url = parse_url( $value['url'], PHP_URL_PATH );
                $arr_pathinfo = pathinfo( $str_file_url );

                $str_file_ext = $arr_pathinfo['basename'];
                $arg_img_exts = [ 'jpg', 'jpeg', 'png', 'gif' ];
                // TODO
                //    if ( in_array($str_file_ext, $arg_img_exts  )) {
                $str_ret .= '<img src="' . esc_url( $value['url'] ) . '"'
                            . $this->global_attrs( $arr_extra['img_global_attrs'] )
                            . ' title="' . esc_url( $value['url'] ) . '"'
                            . ' alt="' . esc_url( $value['url'] ) . '">';
                //    }

                $str_ret .= $str_click_close;
                if ( $arr_type_args['img_view_msg'] !== false ) {
                    $str_ret .= '<p class="wpezmeta-desc ">'
                                . esc_html( $arr_type_args['img_view_msg'] ) . '</p>';
                }
                $str_ret .= '</span>';
            }

            // Add the remove checkbox
            $str_rm_msg = '';
            if ( $arr_type_args['remove_msg'] !== false ) {
                $str_rm_msg = ' title="' . esc_attr( $arr_type_args['remove_msg'] ) . '"';
            }
            $str_ret .= '<span ' . $this->global_attrs( $arr_extra['file_delete_global_attrs'] ) . '"' . $str_rm_msg . '>'
                        . '<input type="checkbox"'
                        . ' name="' . esc_attr( $arr_field['name'] ) . esc_attr( $arr_extra['name_file_delete_suffix'] ) . '"'
                        . ' value="' . sanitize_html_class( $arr_field['name'] ) . '">'
                        . '</span>';

            $str_ret .= '</div>';

            if ( $bool_echo !== false ) {
                echo $str_ret;
            }

        }

        return $str_ret;
    }


    protected function getTarget( $arr = [], $key = '' ) {

        $str_targ = '';
        if ( isset( $arr[ $key ] ) && $arr[ $key ] !== false ) {
            $str_targ = ' target="' . esc_attr( $arr[ $key ] ) . '" ';
        }

        return $str_targ;
    }

}