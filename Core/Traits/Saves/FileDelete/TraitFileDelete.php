<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\FileDelete;

trait TraitFileDelete
{


    protected function file_delete($arr_field = [], $wp_object_id = false, $str_meta_type = '', $str_name = false, $str_meta_key = false, $arr_type_args = [])
    {

        // delete_metadata ( $meta_type, $object_id, $meta_key, $meta_value, $delete_all )
        if ( isset ($_POST[ $str_name . trim($arr_type_args['file_delete']) ]))
        {
            //$str_wp_meta_delete = $arr_wp_meta_crud['delete'];

            //$str_wp_meta_delete($wp_obj_id, $str_meta_key);
            delete_metadata ( $str_meta_type, $wp_object_id, $str_meta_key);
            //
            // $str_wp_meta_delete($wp_obj_id, $str_meta_key . trim($arr_type_args['wp_attachment_id']));

            $str_meta_key_id = $str_meta_key . trim($arr_type_args['wp_attachment_id']);
            delete_metadata ( $str_meta_type, $wp_object_id, $str_meta_key_id );
            // TODO - meta delete returns what?
            return true;
        }
        return false;
    }
}
