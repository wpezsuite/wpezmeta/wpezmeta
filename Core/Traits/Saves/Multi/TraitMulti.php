<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\Multi;


trait TraitMulti {

    use \WPezSuite\WPezMeta\Core\Traits\Saves\Sanitizer\TraitSanitizer;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Shared\TraitShared;

    /**
     * TODO - this feels messy. revisit (and refactor?)
     *
     * @param array  $arr_field
     * @param string $wp_obj_id
     * @param        $arr_wp_meta_crud
     *
     * @return string
     */
    public function save( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $str_name     = $arr_field['name'];
        $str_meta_key = $arr_field['register_meta']['meta_key'];

        if ( isset( $_POST[ $str_name ] ) ) {
            $arr_field['value'] = $_POST[ $str_name ];
        }

        // if single => true then save to a single meta row
        if ( ! isset( $arr_field['register_meta']['args']['single'] ) || ( isset( $arr_field['register_meta']['args']['single'] ) && $arr_field['register_meta']['args']['single'] === true ) ) {

            //   $bool_cleanup = true;
            // single === true ? make the $_POST array into a string. we only need one meta row to store it.
            // TODO - sanitize? prior to serialize?
            $arr_field['value'] = '';
            if ( isset( $_POST[ $str_name ] ) ) {
                $arr_field['value'] = serialize( $_POST[ $str_name ] );
            }
            $new_value     = $this->sanitize( $arr_field, $wp_object, $wp_object_id );
            $str_meta_type = $arr_field['register_meta']['object_type'];

            $this->saveCleanup( $new_value, $wp_object_id, $str_meta_type, $str_meta_key );

            // else single !== true
        } elseif ( isset( $arr_field['type_args']['options'] ) && is_array( $arr_field['type_args']['options'] ) ) {
            // it's not a single - we're going to give each check its own meta row.
            // good for when you need to query this meta

            $arr_posted = [];
            if ( isset( $_POST[ $str_name ] ) && is_array( $_POST[ $str_name ] ) ) {
                $arr_posted = $_POST[ $str_name ];
            }

            // $str_wp_meta_add = $arr_wp_meta_crud['add'];
            // delete everything for this obj + meta_key
            //  $str_wp_meta_delete = $arr_wp_meta_crud['delete'];
            //  $str_wp_meta_delete( $wp_obj_id, $str_meta_key );

            $str_meta_type = $arr_field['register_meta']['object_type'];
            //
            delete_metadata( $str_meta_type, $wp_object_id, $str_meta_key );
            // refill
            foreach ( $arr_field['type_args']['options'] as $val => $checked ) {
                // only accept a POST[] value if it's an options
                if ( in_array( $val, $arr_posted ) ) {
                    $arr_field['value'] = $val;

                    // sanitize it
                    $new_value = $this->sanitize( $arr_field, $wp_object, $wp_object_id );
                    // add it
                    add_metadata( $str_meta_type, $wp_object_id, $str_meta_key, $new_value, false );
                }
            }
        }

        return ''; //'TODO';

    }

}