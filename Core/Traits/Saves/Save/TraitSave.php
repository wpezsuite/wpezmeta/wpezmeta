<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\Save;

trait TraitSave
{
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Sanitizer\TraitSanitizer;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Shared\TraitShared;

    public function save($arr_field = [],  $wp_object = false, $wp_object_id = false, $bool_use_default_value = false )
    {

        $str_name = $arr_field['name'];

        if (isset($_POST[$str_name])) {
            $arr_field['value'] = $_POST[$str_name];
        }

        $new_value = $this->sanitizer($arr_field, $wp_object, $wp_object_id);
        $str_meta_key = $arr_field['register_meta']['meta_key'];
        $str_meta_type = $arr_field['register_meta']['object_type'];

        $this->saveCleanup($new_value, $wp_object_id, $str_meta_type, $str_meta_key, $bool_use_default_value );

        return; // TODO ? return something else?
    }

}