<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\Sanitizer;


trait TraitSanitizer
{

    protected function sanitizer($arr_field = [], $wp_object = false, $wp_object_id = false)
    {

        if ( ! isset($arr_field['value']) ){
            return '';
        }

        if ( isset($arr_field['sanitize']) ){

            // true means we'll filter *if* there's a filter
            if ( $arr_field['sanitize'] === true ){

                $str_filter = 'wpezmeta_sanitize_' . trim($arr_field['name']) . '_filter';
                if ( has_filter($str_filter) ) {
                    $arr_field = apply_filters($str_filter, $arr_field, $wp_object, $wp_object_id);
                    return $arr_field['value'];
                }

            // string = use a different method from the (core) Sanitize class
            } elseif( is_string($arr_field['sanitize']) ){
                // if sanitize is a string then check to see if that method exist in the sanitize class
                if ( Sanitize::method_exits($arr_field['sanitize'])){

                    return Sanitize::sanitize( $arr_field['sanitize'], $arr_field['value']);
                }

            // array = custom Sanitize. [0] = instance of the class, [1] = the sanitize method
            } elseif ( is_array(($arr_field['sanitize']))) {
                // TODO this needs to be tested!!
                if ( isset($arr_field['sanitize'][0], $arr_field['sanitize'][1] )) {

                    if ( method_exists($arr_field['sanitize'][0], $arr_field['sanitize'][1]) ){
                        $obj = $arr_field['sanitize'][0];
                        $method = $arr_field['sanitize'][1];
                        return $obj->$method($arr_field, $wp_object, $wp_object_id);
                    }
                }

            // WHOA!!! Be careful cowboy. No sanitize is dangerous.
            } elseif ( $arr_field['sanitize'] === false ){
                // oh no! no sanitization?!?!? ARE YOU SURE?!?!?
                return $arr_field['value'];
            }
        }
        // if ['sanitize'] isn't set or the above fails at some point, use the type's sanitize() method
        return $this->sanitize($arr_field, $wp_object, $wp_object_id);
    }

}