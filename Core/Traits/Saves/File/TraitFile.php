<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\File;

trait TraitFile
{
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Sanitizer\TraitSanitizer;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\FileDelete\TraitFileDelete;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Shared\TraitShared;

    protected static $_wp_image_sizes = false;

    public function save($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false)
    {

        $arr_type_args = $arr_field['type_args'];
        $str_name = trim( $arr_field['name'] );
        $str_meta_key = trim($arr_field['register_meta']['meta_key'] );
        $str_meta_type = trim($arr_field['register_meta']['object_type'] );

        if ( $this->file_delete($arr_field, $wp_object_id, $str_meta_type, $str_name, $str_meta_key, $arr_type_args)){
            return;
        }

        if ( isset ($_FILES[$str_name]) && ! empty($_FILES[$str_name]['tmp_name']) )
        {

            $str_files_name          = $_FILES[$str_name]['name'];
            $str_files_name_tmp_name = $_FILES[$str_name]['tmp_name'];

            // https://github.com/tommcfarlin/WordPress-Upload-Meta-Box/blob/master/plugin.php
            // TODO - check for multiiple

            // Upload the goal image to the uploads directory, resize the image, then upload the resized version
            // todo
            $get_remote = wp_remote_get($str_files_name_tmp_name, $arr_type_args['wp_remote_get_args']);
            if (is_wp_error($get_remote)) {

                if ($arr_type_args['file_source_local'] && file_exists($str_files_name_tmp_name))
                {
                    $get_remote = file_get_contents($str_files_name_tmp_name);

                } else {
                    // TODO - non-local files need any extra level of sec check.
                }
                // TODO check for error and try CURL
                // ref: https://tommcfarlin.com/wp_remote_get/
            }

            $arr_wp_upload_bits = wp_upload_bits(
                $str_files_name,
                null,
                $get_remote,
                $arr_type_args['wp_upload_bits_time']
            );
            // Set post meta about this image. Need the comment ID and need the path.
            if ($arr_wp_upload_bits['error'] == false) {

                $str_wp_upload_bits_url = Sanitize::sanitize( 'esc_url', $arr_wp_upload_bits['url']);

                // Since we've already added the key for this, we'll just update it with the file.
                update_metadata ( $str_meta_type, $wp_object_id, $str_meta_key,  $str_wp_upload_bits_url );

                // -----------
                // ??? https://codex.wordpress.org/Function_Reference/media_handle_upload
                // 	https://core.trac.wordpress.org/ticket/15311
                // !!! https://developer.wordpress.org/reference/functions/wp_get_image_editor/
                // >>>> https://metabox.io/meta-box/

                $upload_dir  = wp_upload_dir(
                    $arr_type_args['wp_upload_dir_time'],
                    $arr_type_args['wp_upload_dir_create_dir'],
                    $arr_type_args['wp_upload_dir_refresh_cache']
                ); // Set upload folder
                $filename    = basename($str_wp_upload_bits_url);
                $wp_filetype = wp_check_filetype($filename, $arr_type_args['wp_check_filetype_mimes']);

                /**
                 * $arr_field['type_args']['ext_whitelist'] = array() of .exts
                 * -- ext in whitelist? include it!
                 */
                $bool_whitelist = true;
                if (is_array($arr_type_args['ext_whitelist'])) {
                    if ( ! in_array($wp_filetype['ext'], $arr_type_args['ext_whitelist']) )
                    {
                        $bool_whitelist = false;
                    }
                }
                /**
                 * $arr_field['type_args']['ext_blacklist'] = array() of .exts
                 * -- ext in blackist? exclude it!
                 */
                $bool_blacklist = true;
                if (is_array($arr_type_args['ext_blacklist'])) {
                    if ( in_array($wp_filetype['ext'], $arr_type_args['ext_blacklist']) )
                    {
                        $bool_blacklist = false;
                    }
                }
                // kinda silly to do it this way but for some reason it helps
                if ( $bool_whitelist == false || $bool_blacklist == false ) {
                    // TODO - some sort of error msg?
                    return;
                }

                // Check folder permission and define file location
                if (wp_mkdir_p($upload_dir['path'])) {
                    $file = $upload_dir['path'] . '/' . $filename;
                } else {
                    $file = $upload_dir['basedir'] . '/' . $filename;
                }

                // do we have an image?
                $arr_sizes_meta = [];
                if (preg_match('!^image/!', $wp_filetype['type'])
                    && file_is_displayable_image($file)
                ) {

                    /**
                     * $arr_field['type_args']['image_sizes']
                     * = null or false - no resizing (save a single image "as is" / as uploaded
                     * = true - use WP registed image sizes
                     * = array() - of images sizes + crop bool
                     */
                    if ($arr_type_args['image_sizes'] !== false) {

                        // array === use custom sizes
                        if (is_array($arr_type_args['image_sizes'])) {
                            $arr_image_sizes = $arr_type_args['image_sizes'];
                            // true === use the registered WP images sizes
                        } else {
                            $arr_image_sizes = $this->wp_image_sizes();
                        }

                        // take the image sizes and make some images
                        $arr_sizes_meta = [];
                        foreach ($arr_image_sizes as $key => $arr_attr) {

                            if ( is_array($arr_attr) ) {
                                {
                                    // merge under the defaults.
                                    $arr_size_args = array_merge($this->image_size_args_defaults(), $arr_attr);
                                }
                                if ($arr_size_args['active'] === false) {
                                    continue;
                                }
                                //	if ( $arr_attrs['active'] !== false ) {
                                $mix_imis = $this->image_make_intermediate_size($file, $arr_size_args);
                                if ($mix_imis !== false) {
                                    $str_size                  = $arr_attr['name'];
                                    $arr_sizes_meta[$str_size] = $mix_imis;
                                }
                            }
                        }
                    }

                    /**
                     * $arr_field['type_args']['thumbnail']
                     * = true - make a thumbnail
                     * = [] - make a thumbnaml with these image size args
                     * = false - don't make a thumbnail
                     */
                    if ($arr_type_args['thumbnail'] !== false) {

                        $arr_size_args = $this->thumbnail_args_defaults();
                        if ( is_array($arr_type_args['thumbnail']) )
                        {
                            $arr_size_args = array_merge($this->thumbnail_args_defaults(), $arr_type_args['thumbnail']);
                        }
                        $mix_imis = $this->image_make_intermediate_size($file, $arr_size_args);;
                        if ($mix_imis !== false)
                        {
                            $arr_sizes_meta['thumbnail'] = $mix_imis;
                        }
                    }
                }

                /**
                 * $arr_field['type_args']['media_library']
                 * - true = add the file to the media library.
                 */
                if ($arr_type_args['media_library'] !== false) {

                    /*
                    // Check folder permission and define file location
                    if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                        $file = $upload_dir['path'] . '/' . $filename;
                    } else {
                        $file = $upload_dir['basedir'] . '/' . $filename;
                    }
                    */

                    $str_title = $this->wp_insert_post_switch($wp_object, $str_files_name, $arr_type_args['attachment_post_title']);
                    $str_content = $this->wp_insert_post_switch($wp_object, $str_files_name, $arr_type_args['attachment_post_content']);

                    // Set attachment data
                    $attachment = [
                        'file'           => $file,
                        'post_mime_type' => $wp_filetype['type'],
                        'post_title'     => sanitize_text_field($str_title),
                        'post_content'   => sanitize_text_field($str_content),
                        'post_status'    => 'inherit',
                        'post_type'      => 'attachment'
                    ];

                    $attach_id = wp_insert_post($attachment, true);

                    if ( $this->type_args_media_library($arr_field) ){
                      //  $str_wp_meta_update($wp_obj_id, $str_meta_key . esc_attr($arr_type_args['wp_attachment_id']), $attach_id);
                        $str_meta_key_id =  $str_meta_key . trim($arr_type_args['wp_attachment_id']);
                        $attach_id = Sanitize::sanitize('sanitize_integer', $attach_id);
                        update_metadata( $str_meta_type, $wp_object_id, $str_meta_key_id, $attach_id );
                    }

                    // https://codex.wordpress.org/Function_Reference/wp_insert_attachment
                    //		$attach_id = wp_insert_attachment( $attachment, $file, $wp_obj_id );

                    // Include image.php
                    require_once(ABSPATH . 'wp-admin/includes/image.php');

                    if ( (isset($arr_type_args['image_sizes'])
                            && is_array($arr_type_args['image_sizes']) )
                        || ( isset($arr_type_args['thumbnail'])
                            && $arr_type_args['thumbnail'] !== false ))
                    {
                        if ( ! isset($attach_data['image_meta'])) {

                            $arr_image_meta = [

                                'aperture'          => 0,
                                'credit'            => '',
                                'camera'            => '',
                                'caption'           => '',
                                'created_timestamp' => 0,
                                'copyright'         => '',
                                'focal_length'      => 0,
                                'iso'               => 0,
                                'shutter_speed'     => 0,
                                'title'             => '',
                                'orientation'       => 0,
                                'keywords'          => []
                            ];
                        } else {
                            $arr_image_meta = $attach_data['image_meta'];
                        }
                        $arr_gis     = getimagesize($file);
                        $attach_data = [
                            'width'      => $arr_gis[0],
                            'height'     => $arr_gis[1],
                            'file'       => $file,
                            'sizes'      => $arr_sizes_meta,
                            'image_meta' => $arr_image_meta,
                            'wpezmeta'   => $str_name
                        ];
                        // since we're creating our own sizes outside normal WP protocol,
                        // we need to create this meta, which is used by the media gallery
                        update_metadata( 'post', $attach_id,  '_wp_attachment_metadata', $attach_data );

                    } else {

                        // Define attachment metadata AND gen the images
                        $attach_data = wp_generate_attachment_metadata( $attach_id, $file);
                        // Assign metadata to attachment
                        wp_update_attachment_metadata($attach_id, $attach_data);
                    }

                    // And finally assign featured image to post
                    if ( $arr_type_args['set_post_thumbnail'] === true) {
                        set_post_thumbnail( $wp_object_id, $attach_id );
                    }

                }
            }

        }
    }

    // TODO - revisit this
    protected function wp_insert_post_switch( $wp_object, $str_files_name = '',  $str_case = '', $args = '' ){

        $str_ret = '';

        switch ($str_case) {

            case false:
                $str_ret = '';
                break;

            case 'file_name':
                // TODO - cle
                $str_ret = $str_files_name;
                break;

            case 'file_name_clean':
                // TODO - remove file ext.
                $filename = pathinfo($str_files_name, PATHINFO_FILENAME);
                $str_ret = preg_replace('/[^a-zA-Z0-9]/',' ', $filename);
                ;
                break;

            case 'name':
                if ( isset($wp_object->name)) {
                    $str_ret = $wp_object->name;
                }
                break;

            case 'post_title':
                if ( isset($wp_object->post_title)) {
                    $str_ret = $wp_object->post_title;
                }
                break;

            default:
                if ( isset($wp_object->post_title)) {
                    $str_ret = $wp_object->post_title;
                }
                // is there another meta field you want to store as the attachment title?
                // note: this works for the initial save. but - for now? - won't be updated
                // if this  field gets updated
                if (isset($_POST[$str_case])) {
                    $str_ret = $_POST[$str_case];
                }
        }
        return $str_ret;
    }


    /**
     * @return array
     */
    protected function thumbnail_args_defaults()
    {

        $arr_def = [
            'active'             => true,
            // 'name'             => false,
            'image_editor_order' => [
                'resize'      => true,
                'set_quality' => true,
                'crop'        => false,
                'rotate'      => false,
                'flip'        => false,
            ],
            'width'              => absint(get_option('thumbnail_size_w', 128)),
            'height'             => absint(get_option('thumbnail_size_h', 96)),
            'crop'               => (bool)get_option('thumbnail_copy', 1),
            'set_quality'        => 90
        ];

        return $arr_def;
    }

    /**
     * @return array
     */
    protected function image_size_args_defaults()
    {

        $arr_def = [
            'active'             => true,
            'name'               => false,
            'image_editor_order' => $this->image_editor_order(),
            'width'              => 9999,
            'height'             => 9999,
            'crop'               => false,
            'set_quality'        => 90,
        ];

        return $arr_def;
    }

    protected function image_editor_order()
    {

        $arr = [
            'resize'      => true,
            'set_quality' => true,
            'crop'        => true,
            'rotate'      => true,
            'flip'        => true,
        ];

        return $arr;
    }

    /**
     * ref:
     * https://codex.wordpress.org/Function_Reference/get_intermediate_image_sizes
     * see examples
     *
     * @return array|bool
     */
    protected function wp_image_sizes()
    {

        global $_wp_additional_image_sizes;

        if (self::$_wp_image_sizes !== false) {
            return self::$_wp_image_sizes;
        }

        $sizes = [];
        foreach (get_intermediate_image_sizes() as $_size) {
            if (in_array($_size,
                         ['thumbnail', 'medium', 'medium_large', 'large'])) {
                $sizes[$_size]['w']    = get_option("{$_size}_size_w");
                $sizes[$_size]['h']    = get_option("{$_size}_size_h");
                $sizes[$_size]['crop'] = (bool)get_option("{$_size}_crop");
            } elseif (isset($_wp_additional_image_sizes[$_size])) {
                $sizes[$_size] = [
                    'w'    => $_wp_additional_image_sizes[$_size]['width'],
                    'h'    => $_wp_additional_image_sizes[$_size]['height'],
                    'crop' => $_wp_additional_image_sizes[$_size]['crop'],
                ];
            }
        }
        self::$_wp_image_sizes = $sizes;

        return $sizes;
    }


    protected function type_args_media_library($arr_field = [])
    {
        // file == only save the attachment id if the field is config'ed for that
        if ( ! isset($arr_field['type_args']['media_library']) || (isset($arr_field['type_args']['media_library'])
                && $arr_field['type_args']['media_library'] !== false) )
        {
            return true;
        }

        return false;
    }


    /**
     * Resizes an image to make a thumbnail or intermediate size. "Forked" from
     * wp-includes/media.php
     *
     * The returned array has the file size, the image width, and image height.
     * The
     * {@see 'image_make_intermediate_size'} filter can be used to hook in and
     * change the values of the returned array. The only parameter is the
     * resized file path.
     *
     * @since 2.5.0
     *
     * @param string $file   File path.
     * @param int    $width  Image width.
     * @param int    $height Image height.
     * @param bool   $crop   Optional. Whether to crop image to specified width
     *                       and height or resize. Default false.
     *
     * @return false|array False, if no image was created. Metadata array on
     *                     success.
     */
    protected function image_make_intermediate_size(
        $file,
        $arr_size_args = false
    ) {

        $editor = wp_get_image_editor($file);

        if (is_wp_error($editor)) {
            return false;
        }

        if ( ! isset($arr_size_args['image_editor_order']) || ! is_array($arr_size_args['image_editor_order']) )
        {
            return false;
        }


        foreach ($arr_size_args['image_editor_order'] as $key => $bool) {

            if ( isset($this->image_editor_order()[$key]) && $this->image_editor_order()[$key] !== false && $bool !== false )
            {
                $mix_ret = $this->$key($editor, $arr_size_args);
            }
        }

        $resized_file = $editor->save();
        if ( ! is_wp_error($resized_file) && $resized_file) {
            unset($resized_file['path']);

            return $resized_file;
        }

        return false;
    }


    protected function resize($editor, $arr_size_args = [])
    {
        return $editor->resize(absint($arr_size_args['width']),
                               absint($arr_size_args['height']),
                               (bool)$arr_size_args['crop']);
    }


    protected function crop($editor, $arr_size_args = [])
    {

        if (isset($arr_size_args['crop'])) {

            $arr_crop_args = $arr_size_args['crop'];

            if (isset($arr_crop_args['src_x'], $arr_crop_args['src_y'], $arr_crop_args['src_w'], $arr_crop_args['src_h'])) {

                $dst_w = null;
                if (isset($arr_crop_args['dst_w'])) {
                    $dst_w = intval($arr_crop_args['dst_w']);
                }
                $dst_h = null;
                if (isset($arr_crop_args['dst_h'])) {
                    $dst_h = intval($arr_crop_args['dst_h']);
                }
                $src_abs = false;
                if (isset($arr_crop_args['src_abs'])) {
                    $src_abs = (boolean)$arr_crop_args['src_abs'];
                }

                return $editor->crop((int)$arr_crop_args['src_x'],
                                     (int)$arr_crop_args['src_y'],
                                     (int)$arr_crop_args['src_w'],
                                     (int)$arr_crop_args['src_h'],
                                     $dst_w,
                                     $dst_h,
                                     $src_abs);
            }
        }
    }


    protected function rotate($editor, $arr_size_args = [])
    {
        // rotate( $angle ) - Rotates current image counter-clockwise by $angle.
        if (isset($arr_size_args['rotate'])) {
            return $editor->rotate((integer)$arr_size_args['rotate']);
        }

        return $editor;
    }


    protected function flip($editor, $arr_size_args = [])
    {
        // flip( $horz, $vert ) - Flips current image on the horizontal or vertical axis.
        if (isset($arr_size_args['flip'])) {

            $horz = false;
            if (isset($arr_size_args['flip']['horz'])) {
                $horz = (boolean)$arr_size_args['flip']['horz'];
            }

            $vert = false;
            if (isset($arr_size_args['flip']['vert'])) {
                $vert = (boolean)$arr_size_args['flip']['vert'];
            }

            return $editor->flip($horz, $vert);
        }

        return $editor;
    }


    protected function set_quality($editor, $arr_size_args = [])
    {
        // Sets Image Compression quality on a 1-100% scale as an integer (1-100). Default quality defined in WP_Image_Editor class is 90.
        if (isset($arr_size_args['set_quality'])) {
            return $editor->set_quality((integer)$arr_size_args['set_quality']);
        }

        return $editor;
    }

}