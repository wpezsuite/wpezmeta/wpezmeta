<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\Shared;

trait TraitShared
{
    protected function saveCleanup($new_value, $wp_object_id, $str_meta_type, $str_meta_key)
    {

        // cleaning it up boss
       // $old = $str_wp_meta_get($wp_obj_id, $str_meta_key, true);

        $old =  get_metadata($str_meta_type, $wp_object_id, $str_meta_key, true);
        if ($old && empty($new_value)) {
           // $str_wp_meta_delete($wp_obj_id, $str_meta_key);
            delete_metadata ( $str_meta_type, $wp_object_id, $str_meta_key );
        } elseif ($old !== $new_value) {
            //  $str_wp_meta_update($wp_obj_id, $str_meta_key, $new_value);
            update_metadata( $str_meta_type, $wp_object_id, $str_meta_key, $new_value );
        }
    }
}
