<?php

namespace WPezSuite\WPezMeta\Core\Traits\Saves\WPMedia;

trait TraitWPMedia {

    use \WPezSuite\WPezMeta\Core\Traits\Saves\Sanitizer\TraitSanitizer;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\FileDelete\TraitFileDelete;
    use \WPezSuite\WPezMeta\Core\Traits\Saves\Shared\TraitShared;

    public function save ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false ) {

        $arr_type_args = $arr_field['type_args'];
        $str_name      = trim( $arr_field['name'] );
        $str_meta_key  = trim( $arr_field['register_meta']['meta_key'] );
        $str_meta_type = trim( $arr_field['register_meta']['object_type'] );

        if ( $this->file_delete( $arr_field, $wp_object_id, $str_meta_type, $str_name, $str_meta_key, $arr_type_args ) ) {
            return;
        }

        if ( isset ( $_POST[ $str_name ] ) && isset ( $_POST[ $str_name . trim( $arr_type_args['wp_attachment_id'] ) ] ) && ! empty( $_POST[ $str_name . trim( $arr_type_args['wp_attachment_id'] ) ] ) ) {

            // save the 'wp_attachment_id'
            if ( $this->type_args_media_library( $arr_field ) ) {

                // $str_wp_meta_update = $arr_wp_meta_crud['update'];
                $new_id = absint( $_POST[ $str_name . trim( $arr_type_args['wp_attachment_id'] ) ] );
                // $str_wp_meta_update( $wp_obj_id, $str_meta_key . trim($arr_type_args['wp_attachment_id']), $new_id );

                $str_meta_key_id = $str_meta_key . trim( $arr_type_args['wp_attachment_id'] );
                update_metadata( $str_meta_type, $wp_object_id, $str_meta_key_id, $new_id );
            }
            // save the URL
            $arr_field['value'] = $_POST[ $str_name ];
            $new_value          = $this->sanitizer( $arr_field, $wp_object, $wp_object_id );
            $this->saveCleanup( $new_value, $wp_object_id, $str_meta_type, $str_meta_key );

            return true;
        }

        return false;
    }


    protected function type_args_media_library ( $arr_field = [] ) {

        // file == only save the attachment id if the field is config'ed for that
        if ( ! isset( $arr_field['type_args']['media_library'] ) || ( isset( $arr_field['type_args']['media_library'] ) && $arr_field['type_args']['media_library'] !== false ) ) {
            return true;
        }

        return false;
    }

}