<?php

namespace WPezSuite\WPezMeta\Core\Traits\Errors\Error;


trait TraitError {

    protected $_error_wrapper_tag          = 'p';
    protected $_error_wrapper_global_attrs = [ 'class' => 'wpezmeta-error' ];

    /**
     * @param array $arr_field
     *
     * @return string
     */
    public function error( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'error', $arr_field['active_blacklist'] ) ) {
            if ( isset( $arr_field['error'] ) && ! empty( esc_attr( $arr_field['error'] ) ) ) {

                $str_tag_open  = '';
                $str_tag_close = '';
                if ( ! empty( esc_attr( $this->_error_wrapper_tag ) ) ) {

                    $str_tag_open = '<' . esc_attr( $this->_error_wrapper_tag ) . '>';
                    if ( ! empty( $this->_error_wrapper_global_attrs ) ) {
                        $str_tag_open = '<' . esc_attr( $this->_error_wrapper_tag ) . $this->global_attrs( $this->_error_wrapper_global_attrs ) . '">';
                    }
                    $str_tag_close = '</' . esc_attr( $this->_error_wrapper_tag ) . '>';
                }
                $str_ret .= $str_tag_open;
                $str_ret .= esc_html( $arr_field['error'] );
                $str_ret .= $str_tag_close;

                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }

}