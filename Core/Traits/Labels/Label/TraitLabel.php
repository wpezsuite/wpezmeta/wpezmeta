<?php
namespace WPezSuite\WPezMeta\Core\Traits\Labels\Label;


trait TraitLabel {

    public function label($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret = '';
        if ( ! in_array('label', $arr_field['active_blacklist']) ) {
            if (isset($arr_field['label']) && ! empty(esc_attr($arr_field['label']))) {

             //   $arr_field['label_global_attrs'] = array_merge_recursive(['id'=>esc_attr( $arr_field['id'] ) . '-label'], $arr_field['label_global_attrs']);

                $str_ret .= '<label ' . $this->global_attrs($arr_field['label_global_attrs']) . '>';
                $str_ret .= esc_html($arr_field['label']);
                $str_ret .= '</label>';

                if ($bool_echo !== false) {
                    echo $str_ret;
                }
            }
        }
        return $str_ret;
    }
}