<?php
namespace WPezSuite\WPezMeta\Core\Traits\Labels\Radio;


trait TraitRadio {

    public function label($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret = '';
        if ( ! in_array('label', $arr_field['active_blacklist']) ) {
            if ($arr_field['label'] !== false) {

                $str_ret .= '<span class="' . esc_attr($this->_class_faux_label) . '">';
                $str_ret .= esc_html($arr_field['label']);

                $str_ret .= '</span>';

                if ($bool_echo !== false) {
                    echo $str_ret;
                }

            }
        }
        return $str_ret;
    }

}