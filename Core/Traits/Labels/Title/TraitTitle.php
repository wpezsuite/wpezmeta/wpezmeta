<?php
namespace WPezSuite\WPezMeta\Core\Traits\Labels\Title;


trait TraitTitle {

    protected $_label_wrapper_global_attrs = ['class' => 'wpezmeta-title'];

    public function label($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret = '';
        if ( ! in_array('label', $arr_field['active_blacklist']) ){

            $str_all_attrs = $this->all_attrs($arr_field);
            // you can use this to add a (font) icon
            if ( ! empty($str_all_attrs) && $str_all_attrs != ' ') {
                $str_ret .= '<span ';
                $str_ret .= $str_all_attrs;
                $str_ret .= '>';
                $str_ret .= '</span> ';
            }

            $str_ret .= '<span ' . $this->global_attrs($this->_label_wrapper_global_attrs) . '>';
            $str_ret .= esc_html($arr_field['label']);
            $str_ret .= '</span>';

            if ($bool_echo !== false) {
                echo $str_ret;
            }
        }
        return $str_ret;
    }

}