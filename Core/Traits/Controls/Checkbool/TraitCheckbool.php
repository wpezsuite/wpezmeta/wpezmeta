<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Checkbool;


trait TraitCheckbool
{

    public function control($arr_field = [], $wp_object = false,  $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret = '';
        if ( ! in_array('control', $arr_field['active_blacklist']) ) {

            if (isset($arr_field['name'])) {

                $value = $this->getValue($arr_field, $wp_object, $wp_object_id, $bool_use_default_value);

                $arr_type_args = $arr_field['type_args'];

                $str_ret .= '<input type="checkbox"';
                $str_ret .= ' name="' . esc_attr($arr_field['name']) . '"';
                $str_ret .= ' value="' . esc_attr($arr_type_args['checked_value']) . '" ';
                if ($value == $arr_type_args['checked_value'] || ($bool_use_default_value === true &&  $arr_field['default_value'] == $arr_type_args['checked_value']) ) {
                    $str_ret .= ' checked="checked" ';
                }
                $str_ret .= $this->all_attrs($arr_field);
                $str_ret .= '>';

                if ($bool_echo !== false) {
                    echo $str_ret;
                }
            }
        }
        return $str_ret;

    }

}