<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\WPEditor;


trait TraitWPEditor {

    public function control ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $ret_ob = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {

            // TODO - evidently wp_editor / tiny mce is VERY picky about it's id.
            $str_id        = $arr_field['name'];
            $arr_type_args = [];
            // TODO type_args defaults for wp_editor
            if ( isset( $arr_field['type_args'] ) && is_array( $arr_field['type_args'] ) ) {
                // TODO defaults? default "presets"  i.e., some set of type args predefined
                $arr_type_args = $arr_field['type_args'];
            }
            $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

            // gotta ob_ this 'ish ;)
            ob_start();
            wp_editor( $value, $str_id, $arr_type_args );
            $ret_ob = ob_get_contents();
            ob_end_clean();

            if ( $bool_echo === true ) {
                echo $ret_ob;
            }
        }

        return $ret_ob;
    }
}
