<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Input;


trait TraitInput {

    public function control( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {
            if ( isset( $arr_field['name'], $arr_field['type'] ) ) {

                $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

                $str_ret .= '<span class="wpezmeta-field-wrapper-inner">';
                $str_ret .= '<input type="' . esc_attr( $arr_field['type'] ) . '"';
                $str_ret .= ' id="' . esc_attr( $arr_field['id'] ) . '"';
                $str_ret .= ' name="' . esc_attr( $arr_field['name'] ) . '"';
                $str_ret .= ' value="' . $this->escape( $value ) . '" ';
                // make sure we're going to render a dlabel for this
                /*
                if ( ! in_array( 'desc', $arr_field['active_blacklist'] ) ) {
                    $str_ret .= ' aria-labelledby="' . esc_attr( $arr_field['id'] ) . '-label"';
                }
                // make sure we're going to render a desc for this
                if ( ! in_array( 'desc', $arr_field['active_blacklist'] ) ) {
                    $str_ret .= ' aria-describedby="' . esc_attr( $arr_field['id'] ) . '-desc"';
                }
                */
                $str_ret .= $this->all_attrs( $arr_field );
                $str_ret .= '>';
                $str_ret .= '</span>';

                $str_ret .= $this->datalist( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value, $bool_echo );

                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }


    protected function datalist( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( isset( $arr_field['html_attrs']['list'] ) && is_string( ( $arr_field['html_attrs']['list'] ) )
             && isset( $arr_field['type_args']['datalist'] )
             && is_array( ( $arr_field['type_args']['datalist'] ) )
             && isset( $arr_field['type_args']['datalist']['options'] )
             && is_array( ( $arr_field['type_args']['datalist']['options'] ) )
        ) {

            $arr_def = [
                'label'       => false,
                'select_name' => false
            ];

            $arr_datalist = array_merge( $arr_def, $arr_field['type_args']['datalist'] );

            $label_open = '';
            // $label_close = '';
            if ( is_string( $arr_datalist['label'] ) ) {

                $label_open = '<label>' . esc_attr( $arr_datalist['label'] ) . '</label>';
            }

            $select_open  = '';
            $select_close = '';
            if ( is_string( $arr_datalist['select_name'] ) ) {

                $select_open  = '<select name="' . esc_attr( $arr_datalist['select_name'] ) . '">';
                $select_close = '</select>';
            }

            $str_options = '';
            foreach ( $arr_datalist['options'] as $option ) {

                if ( ! empty( esc_attr( $option ) ) ) {
                    $str_options .= '<option value="' . esc_attr( $option ) . '"></option>';
                }
            }

            if ( ! empty( $str_options ) ) {

                $str_ret .= '<datalist id="' . esc_attr( $arr_field['html_attrs']['list'] ) . '">';

                $str_ret .= $label_open;
                $str_ret .= $select_open;

                $str_ret .= $str_options;

                $str_ret .= $select_close;
                $str_ret .= '</datalist>';
            }

        }

        return $str_ret;

    }

}