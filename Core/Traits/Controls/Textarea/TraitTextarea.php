<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Textarea;

trait TraitTextarea {

    public function control ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {
            if ( isset( $arr_field['name'], $arr_field['type'] ) ) {

                $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

                $str_ret .= '<textarea';
                $str_ret .= ' name="' . esc_attr( $arr_field['name'] ) . '" ';
                $str_ret .= $this->all_attrs( $arr_field );
                $str_ret .= '>';
                $str_ret .= $this->escape( $value );
                $str_ret .= '</textarea>';

                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }

}
