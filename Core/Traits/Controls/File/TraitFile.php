<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\File;

trait TraitFile
{

    public function control($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret        = '';
        if ( ! in_array('control', $arr_field['active_blacklist']) ) {

            // what type of file
            $str_type = 'file';
            if ($arr_field['type'] == 'wp_media') {
                $str_type = 'wp_media';
            }

        //    $arr_type_args = $this->type_args($arr_field);

            $str_name       = esc_attr($arr_field['name']);
            $str_input_type = 'text';
            if ( $arr_field['type_args']['input_text_url'] === false) {
                $str_input_type = 'hidden';
            }

            $arr_ga  = [
                'type'  => $str_input_type,
                'id'    => $str_name . '_file_url_id',
                'class' => 'wpezmeta-element-file-url wpezmeta-element-file-url-' . $str_type,
                'name'  => $str_name . '_file_url'
            ];
            $str_ret .= '<input ' . $this->global_attrs($arr_ga) . '/>';

            $str_ret .= $this->elementFileCustom($arr_field, $wp_object, $wp_object_id, $bool_use_default_value);

            $value = $this->getValue($arr_field, $wp_object, $wp_object_id, $bool_use_default_value);

            $arr_ga  = [
                'type'  => 'hidden',
                'id'    => $str_name . '_id',
                'name'  => $str_name . '_id',
                'value' => $value['id']
            ];
            $str_ret .= '<input ' . $this->global_attrs($arr_ga) . '/>';

            if ($bool_echo !== false) {
                echo $str_ret;
            }
        }
        return $str_ret;
    }

}