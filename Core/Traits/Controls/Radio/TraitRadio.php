<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Radio;


trait traitRadio {

    public function control ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {
            if ( isset( $arr_field['type_args']['options'] ) && is_array( $arr_field['type_args']['options'] ) ) {

                //   $str_id_fs = $arr_field['name'] . '_fieldset ';
                //   $str_ret = '<fieldset id="' . esc_attr($str_id_fs) .'" ' . $this->global_attrs($this->trait_element_radio_defaults()['fieldset_global_attrs']) . '>';
                //    $str_ret .= '<legend class="screen-reader-text">' . esc_html( $arr_field['label'] ) . '</legend>';

                $str_id_fs = $arr_field['name'] . '_fieldset';
                $str_ret   .= '<fieldset id="' . esc_attr( $str_id_fs ) . '" ' . $this->global_attrs( $arr_field['type_args']['fieldset_global_attrs'] ) . '>';
                $str_ret   .= '<legend ' . $this->global_attrs( $arr_field['type_args']['legend_global_attrs'] ) . '">';
                $str_ret   .= esc_html( $arr_field['type_args']['legend'] );
                $str_ret   .= '</legend>';

                $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

                foreach ( $arr_field['type_args']['options'] as $val => $choice ) {

                    $str_ret .= '<span ' . $this->global_attrs( $arr_field['type_args']['ctrl_wrapper_global_attrs'] ) . '>';

                    $arr_ga = [
                        'type'  => "radio",
                        'name'  => $arr_field['name'],
                        'value' => $val,
                        'id'    => $arr_field['id'],
                        'class' => str_replace( '_', '-', $arr_field['name'] . '-class' )
                    ];

                    if ( isset( $arr_field['global_attrs'] ) && is_array( $arr_field['global_attrs'] ) ) {
                        $arr_field['global_attrs'] = array_merge( $arr_field['global_attrs'], $arr_ga );
                    }
                    else {
                        $arr_field['global_attrs'] = $arr_ga;
                    }

                    $str_ret .= '<input ';
                    if ( $val == $value ) {
                        $str_ret .= ' checked="checked" ';
                    }
                    $str_ret .= $this->all_attrs( $arr_field );
                    $str_ret .= '>';

                    $str_ret .= '<label for="' . esc_attr( $arr_field['id'] ) . '">';
                    $str_ret .= esc_html( $choice );
                    $str_ret .= '</label>';
                    $str_ret .= '</span>';

                }
                $str_ret .= '</fieldset>';
                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }


}