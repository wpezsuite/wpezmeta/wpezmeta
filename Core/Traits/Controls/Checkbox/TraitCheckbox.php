<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Checkbox;


trait TraitCheckBox {

    public function control ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {

            if ( isset( $arr_field['type_args']['options'] ) && is_array( $arr_field['type_args']['options'] ) ) {

                $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

                $arr_value = [];
                if ( is_array( $value ) ) {
                    $arr_value = $value;
                }

                $str_id_fs = $arr_field['name'] . '_fieldset';
                $str_ret   .= '<fieldset id="' . esc_attr( $str_id_fs ) . '" ' . $this->global_attrs( $arr_field['type_args']['fieldset_global_attrs'] ) . '>';
                $str_ret   .= '<legend ' . $this->global_attrs( $arr_field['type_args']['legend_global_attrs'] ) . '">';
                $str_ret   .= esc_html( $arr_field['type_args']['legend'] );
                $str_ret   .= '</legend>';


                foreach ( $arr_field['type_args']['options'] as $val => $text ) {

                    $str_checked = '';
                    // TODO - use default is no $value
                    if ( in_array( $val, $arr_value ) ) {
                        $str_checked = ' checked="checked" ';
                    }

                    $str_ret .= '<span ' . $this->global_attrs( $arr_field['type_args']['ctrl_wrapper_global_attrs'] ) . '>';

                    $str_id = sanitize_key( esc_attr( $arr_field['name'] ) . '-'
                                            . sanitize_html_class( $val ) );
                    $str_id = str_replace( '_', '-', strtolower( $str_id ) );
                    $arr_ga = [
                        'type'  => "checkbox",
                        'value' => $val,
                        'id'    => $str_id
                    ];
                    if ( isset( $arr_field['global_attrs'] ) && is_array( $arr_field['global_attrs'] ) ) {
                        $arr_field['global_attrs'] = array_merge( $arr_field['global_attrs'], $arr_ga );

                    }
                    else {
                        $arr_field['global_attrs'] = $arr_ga;
                    }
                    $str_ret .= '<input name="' . esc_attr( $arr_field['name'] ) . '[]" ';
                    $str_ret .= $str_checked . ' ';
                    $str_ret .= $this->all_attrs( $arr_field );
                    $str_ret .= '>';

                    $str_ret .= '<label for="' . esc_attr( $str_id ) . '">'
                                . $this->escape( $text ) . '</label>';
                    $str_ret .= '</span>';
                }
                $str_ret .= '</fieldset>';;

                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }

}