<?php

namespace WPezSuite\WPezMeta\Core\Traits\Controls\Select;


trait TraitSelect {

    public function control ( $arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true ) {

        $str_ret = '';
        if ( ! in_array( 'control', $arr_field['active_blacklist'] ) ) {
            if ( isset( $arr_field['name'], $arr_field['type'], $arr_field['type_args']['options'] )
                 && is_array( $arr_field['type_args']['options'] )
            ) {

                $value = $this->getValue( $arr_field, $wp_object, $wp_object_id, $bool_use_default_value );

                $str_ret .= '<select ';
                $str_ret .= 'name="' . esc_attr( $arr_field['name'] ) . '" ';
                $str_ret .= 'value="' . $this->escape( $value ) . '" ';
                $str_ret .= $this->all_attrs( $arr_field );
                $str_ret .= '>';

                $str_opts = '';
                foreach ( $arr_field['type_args']['options'] as $val => $choice ) {

                    $str_checked = '';
                    if ( $val == $value ) {
                        $str_checked = ' selected="selected"';
                    }

                    $str_opts .= '<option';
                    $str_opts .= ' value="' . $this->escape( $val ) . '"';
                    $str_opts .= $str_checked;
                    $str_opts .= '>';
                    $str_opts .= esc_html( $choice );
                    $str_opts .= '</option>';
                }

                $str_ret .= $str_opts;
                $str_ret .= '</select>';

                if ( $bool_echo !== false ) {
                    echo $str_ret;
                }
            }
        }

        return $str_ret;
    }


}