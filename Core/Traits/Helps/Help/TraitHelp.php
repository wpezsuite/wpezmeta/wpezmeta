<?php
namespace WPezSuite\WPezMeta\Core\Traits\Helps\Help;


trait TraitHelp {

    /**
     * Ref: https://chrisbracco.com/a-simple-css-tooltip/
     *
     * @param array $arr_field
     *
     * @return string
     */
    public function help($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true)
    {

        $str_ret = '';
        if ( ! in_array('help', $arr_field['active_blacklist']) ){

            if ( isset($arr_field['help']) && ! empty(esc_attr($arr_field['help']))) {

                $str_ret = '<span ' . $this->global_attrs($this->traitHelpDefaults()['wrap_global_attrs']) . '>';
                $str_ret .= '<a href="#"' . esc_attr($this->traitHelpDefaults()['data_tooltip']) . '="' . esc_html($arr_field['label']) . ' - ' . wp_kses_post($arr_field['help']) . '">';
                $str_ret .= '<span ' . $this->global_attrs($this->traitHelpDefaults()['icon_global_attrs']) . '></span>';
                $str_ret .= '</a>';
                $str_ret .= '</span>';

                if ($bool_echo !== false) {
                    echo $str_ret;
                }
            }
        }
        return $str_ret;
    }

    /**
     * TODO
     *
     * @return array
     */
    public function traitHelpDefaults(){

        $arr = [
            'wrap_global_attrs' => [
                'class' => 'wpezmeta-tooltip-wrapper'
            ],
            'data_tooltip' => 'data-tooltip',
            'icon_global_attrs' => [
                'class' => 'dashicons dashicons-editor-help'
            ]
        ];
        return $arr;
    }

}