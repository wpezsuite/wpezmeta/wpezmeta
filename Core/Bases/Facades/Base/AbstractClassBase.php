<?php

namespace WPezSuite\WPezMeta\Core\Bases\Facades\Base;

//use \WPezSuite\WPezMeta\Sanitize as Sanitize;

abstract class AbstractClassBase {

    use \WPezSuite\WPezMeta\Core\Traits\Enqueues\WPezMeta\TraitWPezMeta;
    use \WPezSuite\WPezMeta\Core\Traits\Enqueues\Hyperform\TraitHyperform;


    public function __construct () {}

}