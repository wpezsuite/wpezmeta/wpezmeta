<?php

namespace WPezSuite\WPezMeta\Core\Bases\Containers\Input;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSuite\WPezMeta\Core\Bases\Containers\Base\AbstractClassBase;

if ( ! class_exists( 'AbstractClassInput' ) ) {
    abstract class AbstractClassInput extends AbstractClassBase {

        //
        use \WPezSuite\WPezMeta\Core\Traits\Preps\Prep\TraitPrep;
        use \WPezSuite\WPezMeta\Core\Traits\Descs\Desc\TraitDesc;
        use \WPezSuite\WPezMeta\Core\Traits\Errors\Error\TraitError;
        use \WPezSuite\WPezMeta\Core\Traits\Escapes\Attr\TraitAttr;
        use \WPezSuite\WPezMeta\Core\Traits\Helps\Help\TraitHelp;
        use \WPezSuite\WPezMeta\Core\Traits\Labels\Label\TraitLabel;
        use \WPezSuite\WPezMeta\Core\Traits\Controls\Input\TraitInput;
        use \WPezSuite\WPezMeta\Core\Traits\Saves\Save\TraitSave;
        use \WPezSuite\WPezMeta\Core\Traits\Gets\Value\TraitValue;


        public function extra($arr_field = [], $str_action = '', $wp_object = false, $bool_use_default_value = false, $bool_echo = true)
        {
            return '';
        }


    }
}

