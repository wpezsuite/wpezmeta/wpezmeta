<?php

namespace WPezSuite\WPezMeta\Core\Bases\Containers\File;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSuite\WPezMeta\Core\Bases\Containers\Input\AbstractClassInput;


if ( ! class_exists( 'AbstractClassFile' ) ) {
    abstract class AbstractClassFile extends AbstractClassInput {

        use \WPezSuite\WPezMeta\Core\Traits\Extras\File\TraitFile;
        use \WPezSuite\WPezMeta\Core\Traits\Controls\File\TraitFile;
        use \WPezSuite\WPezMeta\Core\Traits\Gets\File\TraitFile;

        /**
         * type_args defaults for type == file
         *
         * @return array
         */
        protected function baseElementDefaults() {

            $arr_defs = [
                'type_args' => [

                    'remove_msg' => 'Check to remove',

                    'file_active'        => true,
                    'file_view_active'   => true,
                    'file_view_msg'      => 'Click link (above) to view file in a new tab',
                    'img_active'         => true,
                    'img_view_active'    => true,
                    'img_view_target'    => '_blank',
                    'img_view_msg'       => 'Click image to view full-size image in a new tab',
                    // do not override these unless you know what you're doing!
                    'file_delete'        => '_file_delete',
                    'wp_attachment_id'   => '_id',
                    'set_post_thumbnail' => false,

                    // 1) some of the above should likely be moved over to the extra section (or move up and out of 'extra' => [...]
                    // 2 ) be VERY careful 'uckin' with these, some are the .js looks for certain selectors. yes, making it more dynamic is a TODO :)
                    // 3) TODO - add other hardcoded selectors (e.g. in trait-extra-file) to this list
                    'extra'              => [
                        'file_view_target' => '_blank',

                        'file_display_global_attrs' => [
                            'class' => 'wpezmeta-file-display'
                        ],
                        'image_display_class'       => 'wpezmeta-image-display',
                        'image_wrap_global_attrs'   => [
                            'class' => 'wpezmeta-image-wrap'
                        ],
                        'img_global_attrs'          => [
                            'class' => 'wpezmeta-height-275px'
                        ],
                        'file_delete_global_attrs'  => [
                            'class' => 'wpezmeta-file-remove-toggle'
                        ],
                        'name_file_delete_suffix'   => '_file_delete'
                    ]
                ],
            ];

            return $arr_defs;
        }


    }
}
