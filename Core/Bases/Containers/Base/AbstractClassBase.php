<?php

namespace WPezSuite\WPezMeta\Core\Bases\Containers\Base;

if ( ! class_exists('AbstractClassBase')) {
    abstract class AbstractClassBase
    {

        use \WPezSuite\WPezCore\Traits\MarkupGeneration\TraitMarkupGeneration;

        protected $_class_screen_reader_text = 'screen-reader-text';
        protected $_class_faux_label = 'wpezmeta-faux-label';

        // if the element (e.g., file, wp-media) adds any special meta the Register_Meta class will look to this method.
        public function registerMeta($arr_field = [], $wp_object = false)
        {
            return [];
        }

        public function scripts($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false)
        {
            return [];
        }


        public function styles($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false)
        {
            return [];
        }


        public function elementDefaults(){
            return [];
        }

        // any special housekeeping before we begin (e.g. type = 'person_name')
        public function prep($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false){
            return $arr_field;
        }

        // render the desc
        abstract public function desc($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        abstract public function error($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        // render() uses escape()
        abstract public function escape($value = '');

        // render the (form) element() - uses getValue()
        abstract public function control($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        // e.g. file / wp_media use extra()
        abstract public function extra($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        // render the help
        abstract public function help($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        // render the labal
        abstract public function label($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false, $bool_echo = true);

        // sanitize() used by sanitizer()
        abstract public function sanitize($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false);

        // Save uses sanitize()
        abstract public function save($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false);

        // gets the value for the meta_key from WP
        abstract public function getValue($arr_field = [], $wp_object = false, $wp_object_id = false, $bool_use_default_value = false);

    }
}