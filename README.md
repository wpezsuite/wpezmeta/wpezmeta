#### Demo and proper docs coming soon.

Until then:

- core / bases / base / base.php - Will show you what goes into making a form element. Within the bases folder you'll find two classes that extend this base. The (shared) traits are in the traits folder. 

- facades / * - Start with add-meta-box or term-meta for now. That'll show you how all the various pieces (read: services) are brought together. Both user meta and comment meta are is coming soon, or at least eventually. 

- form-elements / * - Are the child classes of each form element. They extend one of the bases are are mainly made up of various traits. Be sure to check out person-name, as it's an example of a composite (i.e., a element made of other elements).  

- services / * - These are the crux of the magic that manage the process from the preprocess'ing onto wrapper or save. . 
  
Sticking to single responsibility was also a priority. That led to a lot of pieces, and that can sometimes be overwhelming, especially when you're just starting to poke around. Eventually the granular structure becomes a blessing as you'll have less and less of a need to peek into the black box(es). Hopefully. 
  
#### Examples 

What gets passed into the (e.g.) AddMetaBox (->ez_loader( $arr_args)) will look something like this:

    $arr_args = array(
    
          // Simple on / off switch
         'active' => true,

          // Not in use (yet)
          'prefix' => $str_prefix,

          // Required - To be used by *_metadata (e.g., get_metadata). Values: comment, post, term, or user. Default: post
          // Also used by Register Meta (if necessary) for the object_type
          'meta_type' => 'post',

          // "Optional" but Required if you want to use services / register_meta
          // 'wp_object_types' => ['wpezmetademo'],

          // Most of the usual WP add_meta_box() $args suspects.
          'add_meta_box' => [
                // Required
                'id' => 'cpt-wpezmeta-demo-meta-box',
                
                // Required
                'title' => __( 'WPezMeta Demo', 'wpezmetademo' ),

                // "Deprecated" by The ezWay :)
                // 'callback' =>

                // Optional - If not specified save_post_post_types (below) will be used
                // 'screen' => ['wpezmetademo'],

                // Optional - Default = advanced
                //'context' => 'optional',

                // Optional - Default = default
                //'priority' => 'optional',

                // callback_args = Also deprecated (probably)
                // callback_args => '?'
          ],

          'wpezfields'    => [
                // Array of field definitions. See example below.
          ],
          
          // Optional. This array will be merged over the (WPezMeta) defaults
          /*
          'wp_nonce'     => [
                'action' => basename( __FILE__ ),
                'name'   => 'TODO',
          ]
          */
          // Required (array). Note: Will be used for screen (above) if screen is not specified
          'save_post_post_types' => ['wpezmetademo']
    );

A typical field - in the wpezfields array (above) - will look something like this:

    $arr_ret['demo_file'] = [
         'active'        => true,
         'name'          => 'demo_file',
         'id'            => optional - if not specified the preprocess (class) will use the name
         'label'         => 'Demo: File',
         'desc'          => 'FYI - You don\'t have to add the image to the WP Media library.',
         'help'          => 'Another uber-cool feature is you can set custom image sizes that will only be applied to this image, AND...wait for it...the image_quality can be set at the individual size level.',
         'type'          => 'file',
         'type_args'     => [
            // essentially the settings / options for a given form element
            // e.g. - Define your image sizes here. See Pros section below
         ],
         
         // Optional (ir = Input Restrictions) - https://www.w3schools.com/html/html_form_input_types.asp Section: Input Restrictions
         'html_ir' => [
            // An array of pairs that will be rendered as 'key' = 'val' in the eventually form element markup
         ],
         
         // Optional - element attributes. The attributes unique to the particular element type
         // e.g., textarea has 'rows' and 'cols' 
         'ele_attrs' => [],
         
         // Optional - ref: https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes
         'global_attrs' => [],
         
         // Optional - ref: https://www.w3schools.com/htmL/html_form_attributes.asp
         'html_attrs' => [],
                         
         'register_meta' => [
              'active'      => true,
              'meta_key'    => 'demo_file',
              // Optional - The services / preprocessor will set this based on the meta_type if it's false
              'object_type' => false,
              'args'        => [
                  // TBD
                  // 'sanitize_callback' => 'sanitize_my_column_meta_key',
                  // TBD
                  // 'auth_callback' => 'authorize_my_column_meta_key',
                  'type'         => 'string',
                  'description'  => 'demo_file',
                  'single'       => true,
                  'show_in_rest' => false,
              ],
         ],
    ];
  

That's it! 

You don't code forms (or in this example, meta boxes) as much as define / configure them. The guts of WPezMeta is the universal code that handles rendering and saving (including nonces). See: facades / add-meta-box / add-meta-box.php. It all starts with the method ez_loader( $arr_args ). 

#### Pros

- Type: checkbox can be configured to not serialize (i.e., save all checks as a single meta row), but instead save each check on its own meta row. This makes it possible to query for individual check values. 

- Type: (each) file form element can be configued for custom images sizes (outside the traditional registered image sizes). In addition: jpg quality, resize, crop, rotate and flip can be set at the *individual size level*. That is, for example, a 500px x 500px can be jpg quality 90, but the 100px x 100px can be jpg quality 70 and also rotated 90 degress.  In short, you have much more control.

- Composite form element types. That is form elements made of other (already defined) elements. Example: form-elements/person-name.
- Bare bones with little if any bloat. 

- Support for your own custom form element types.
 
- There's a service (class) for registerMeta(). 

- OOP. Very DRY. Lots of single responsibility. A very modular architecture.

#### Cons

- Only the basic form element types, at least at the moment. As (client) needs arise more will certainly be added. 

- No repeatable groups, at least not yet. Honestly, this might be outside the scope / sweet spot of this effort.   

- Only HTM5 frontend validation. There's a working proof of concept for server-side validation but that's a ways off.
  
- OOP. Traits. It's certainly a great use case for traits but some haters are sure to hate.

#### FAQ

- **Do you have a working example?** Not yet, but that's coming soon. 


#### Backstory

This all started a few months ago while working on a custom plugin. Those patterns (i.e., a text input is a text input is a text input) grew into this sub-project. A sub-project that turned out to be no sub-project at all. If nothing else, this is detailed and ambitious. 

If I knew then what I know now I'm not so sure I'd do it again :) That said, this has been and continues to be a massive learning experience. I wanted a simple light-weight tool and now I have that. Perhaps it will grow to become more, but even if it doesn't I'm still proud of this product. 


  
 