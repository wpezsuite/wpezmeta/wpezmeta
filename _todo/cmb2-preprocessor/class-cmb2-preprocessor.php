<?php

namespace WPez\WPezMeta;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'CMB2_Preprocessor' ) ) {
	class CMB2_Preprocessor {

		function __construct() {
		}


		/**
		 * @return stdClass
		 */
		protected function file_constants() {

			$obj = new \stdClass();

			$obj->url         = plugin_dir_url( __FILE__ );
			$obj->path        = plugin_dir_path( __FILE__ );
			$obj->path_parent = dirname( $this->_path );
			$obj->basename    = plugin_basename( __FILE__ );
			$obj->file        = __FILE__;

			return $obj;
		}


		protected function ez_defaults_metabox() {

			$arr_defs = array(
				'active' => true
			);

		}

		public function ez_loader( $arr_args_box, $arr_args_fields, $str_prefix = '' ){

			// TODO - other quick / basic validation
			if ( ( isset($arr_args_box['active']) && $arr_args_box['active'] === false )  || ! is_array($arr_args_fields) || empty($arr_args_fields) ){
				return new \WP_Error('WPezClasses\CMB2\Preprocessor' , 'ez_loader() failed - check your args');
			}

			$obj_cmb2 = $this->ez_new_cmb2_box($arr_args_box);

			if ( ! $obj_cmb2 instanceof \CMB2 ){
				return new \WP_Error('WPezClasses\CMB2\Preprocessor' , 'new_cmb2_box() failed - check your args.');
			}

			$str_prefix = esc_attr($str_prefix);

			return $this->ez_add_fields($obj_cmb2, $arr_args_fields, $str_prefix);
		}


		public function ez_new_cmb2_box($arr_args_box = false ){

			if ( is_array($arr_args_box) && ! empty($arr_args_box) ){
				$obj_cmb2 = new_cmb2_box($arr_args_box);
				return $obj_cmb2;
			}
			return false;
		}

		public function ez_add_fields( $obj_cmb2 = false, $arr_args_fields = false, $str_prefix = '' ) {

			// TODO - test the box obj?
			if ( is_array( $arr_args_fields ) && ! empty( $arr_args_fields ) ) {

				foreach ( $arr_args_fields as $key => $arr_val ) {
					// active?
					if ( isset( $arr_val['active'] ) && $arr_val['active'] !== false ) {
						// type field?
						if ( isset( $arr_val['type'] ) && $arr_val['type'] == 'field' ) {
							// get the cmb2 args
							if ( isset( $arr_val['cmb2'] ) && is_array( $arr_val['cmb2'] )  ) {

								// map the ez name to the cmb2 id?
								if ( ! isset($arr_val['cmb2']['id']) && isset($arr_val['ez']['name']) ){
									// in short, the cmb2 settings (essentially) override the (universal) ez args
									$arr_val['cmb2']['id'] = $arr_val['ez']['name'];
								}
								if ( isset($arr_val['cmb2']['id'])){
									$arr_val['cmb2']['id'] =  $str_prefix . trim($arr_val['cmb2']['id']);
								} else {
									// no ez name? no cmb2 id? no good!
									continue;
								}

								// what we call a label, cmb2 calls a name
								if ( ! isset($arr_val['cmb2']['name']) && isset($arr_val['ez']['label']) ){
									// in short, the cmb2 settings (essentially) override the (universal) ez args
									$arr_val['cmb2']['name'] = $arr_val['ez']['label'];
								}

								// which desc?
								if ( ! isset($arr_val['cmb2']['desc']) && isset($arr_val['ez']['desc']) ){
									// in short, the cmb2 settings (essentially) override the (universal) ez args
									$arr_val['cmb2']['desc'] = $arr_val['ez']['desc'];
								}

								$obj_cmb2->add_field( $arr_val['cmb2'] );
							}
						// type group?
						} elseif ( isset( $arr_val['type'] ) && $arr_val['type'] == 'group' ) {
							// get the group
							if ( isset($arr_val['group']) && is_array($arr_val['group']) ) {
								$this->ez_add_fields( $obj_cmb2, $arr_val['group'], $str_prefix );
							}
						}
					}
				}
				return true;
			}
			false;
		}

	}
}

// $test = new Preprocessor();

/*************

$def = array(
	'a' => array(
		'active'  => true, // false,
		'type'    => 'field',
		'cmb2_ez' => array(),
		'cmb2'    => array(),
		'acf_ez'  => array(),
		'acf'     => array()
	),

	'b' => array(
		'active' => true, // false,
		'type'   => 'group',
		'group'  => array(
			'x' => array(
				'active'  => true / false,
				'type'    => 'field',
				'cmb2_ez' => array(),
				'cmb2'    => array(),
				'acf_ez'  => array(),
				'acf'     => array()
			),
			'y' => array(
				'active'  => true / false,
				'type'    => 'field',
				'cmb2_ez' => array(),
				'cmb2'    => array(),
				'acf_ez'  => array(),
				'acf'     => array()
			),

		),
	)
);

*********************/



/**
 * - group
 * -- active
 * -- field 1
 * ---- active
 * ---- ez_cmb2 = array()
 * ---- cmb2 = array()
 * ---- ez_acf = array()
 * ---- acf = ?
 *
 * -- field 2
 * ---- active
 * ---- ez_cmb2 = array()
 * ---- cmb2 = array()
 * ---- ez_acf = array()
 * ---- acf = ?
 *
 *
 * - metabox
 * -- ez
 * -- cmb2
 * -- acf
 * -- other?
 * -- fields
 * ---- fields
 */