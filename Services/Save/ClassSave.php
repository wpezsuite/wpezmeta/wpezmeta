<?php

namespace WPezSuite\WPezMeta\Services\Save;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassSave {

    protected $_wpezfields = [];
    protected $_wp_object;
    protected $_wp_object_id;


    /**
     * @param array $arr_args
     */
    public function ezLoader( $arr_args = [] ) {

        $this->_wpezfields   = $arr_args['wpezfields'];
        $this->_wp_object    = $arr_args['wp_object'];
        $this->_wp_object_id = $arr_args['wp_object_id'];
    }


    /**
     * @param array $arr_args
     *
     * @return string
     */
    public function goSave( $arr = false ) {

        // TODO - revist how Save > loader + process is done. This works - in a pseudo recursive sorta way - but feels pretty wonky now.
        $arr_wpezfields = $this->_wpezfields;
        if ( is_array( $arr ) ) {
            $arr_wpezfields = $arr;
        }


        foreach ( $arr_wpezfields as $key => $arr_field ) {

            if ( $arr_field['active'] !== false ) {

                if ( $arr_field['type'] == 'collection' ) {

                    $arr_field['active'] = false;
                    continue;
                }

                if ( ! isset( $arr_field['this_object'] ) || $arr_field['this_object'] === false ) {

                    $arr_field['active'] = false;
                    continue;
                }

                // collection
                if ( isset( $arr_field['type_args']['wpezfields'] ) && is_array( $arr_field['type_args']['wpezfields'] ) ) {
                    $arr_temp = $this->goSave( $arr_field['type_args']['wpezfields'] );
                } else {
                    // it's a field!
                    $new_obj = $arr_field['this_object'];
                    if ( ! in_array( 'save', $arr_field['active_blacklist'] ) ) {
                        $new_obj->save( $arr_field, $this->_wp_object, $this->_wp_object_id );
                    }
                }
            }
        }

        return $arr_wpezfields;
    }

}
