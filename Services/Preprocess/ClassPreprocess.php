<?php

namespace WPezSuite\WPezMeta\Services\Preprocess;

// No WP - Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassPreprocess {

    protected $_namespace = 'WPezSuite';

    protected $_active                = true;
    protected $_active_blacklist      = [];
    protected $_label                 = '';
    protected $_desc                  = '';
    protected $_help                  = '';
    protected $_wrapper_class_default = 'wpezmeta-field-wrapper';
    protected $_meta_type             = 'post';

    protected $_register_meta_active            = true;
    protected $_register_meta_object_type       = 'post';
    protected $_register_meta_args_type         = 'string';
    protected $_register_meta_args_single       = true;
    protected $_register_meta_args_show_in_rest = false;

    protected $_this_element = [
        'assembled' => false,
        'label'     => false,
        'error'     => false,
        'help'      => false,
        'error'     => false,
        'desc'      => false,
        'element'   => false,
        'extra'     => false,
    ];


    public function __construct() {
    }


    public function setNamespace( $str ) {

        $this->_namespace = $str;
    }


    public function setActive( $bool ) {

        $this->_active = (bool)$bool;
    }

    public function setActiveBlacklist( $arr = false ) {

        if ( is_array( $arr ) ) {
            $this->_active_blacklist = $arr;
        }
    }

    public function setLabel( $str ) {

        $this->_label = (string)$str;
    }

    public function setDesc( $str ) {

        $this->_desc = (string)$str;
    }

    public function setHelp( $str ) {

        $this->_help = (string)$str;
    }

    public function setWrapperClassDefault( $str ) {

        $this->_wrapper_class_default = (string)$str;
    }

    public function setMetaType( $str ) {

        $this->_meta_type = (string)$str;
    }

    public function setRegisterMetaActive( $bool ) {

        $this->_register_meta_active = (bool)$bool;
    }

    public function setRegisterMetaObjectType( $str ) {

        $this->_register_meta_object_type = (string)$str;
    }

    public function setRegisterMetaArgsType( $str ) {

        $this->_register_meta_args_type = (string)$str;
    }

    public function setRegisterMetaArgsSingle( $bool ) {

        $this->_register_meta_args_single = (bool)$bool;
    }

    public function setRegisterMetaArgsShowInRest( $bool ) {

        $this->_register_meta_args_show_in_rest = (bool)$bool;
    }

    public function setThisElement( $arr ) {

        if ( is_array( $arr ) ) {
            $this->_this_element = array_merge( $this->_this_element, $arr );
        }
    }


    /**
     * @param array $arr_args
     *
     * @return string
     */
    public function ezLoader( $arr_wpezfields = [], $arr_parent = false ) {

        if ( ! is_array( $arr_wpezfields ) ) {
            return false;
        }

        $arr_pre = [];
        foreach ( $arr_wpezfields as $key => $arr_field ) {

            $arr_field = array_merge( [ 'active' => true ], $arr_field, $this->thisSpecial() );

            // name && type are both required
            if ( ! isset( $arr_field['name'] ) || ! isset( $arr_field['type'] ) ) {
                $arr_field['active'] = false;
                // TODO - other errors
                $arr_field['error'] = 'e100';
                continue;
            }

            if ( $arr_field['active'] !== false ) {

                // $arr_field['type'] = strtolower( $arr_field['type'] );

                if ( $arr_field['type'] == 'collection' && isset( $arr_field['type_args']['wpezfields'] ) && is_array( $arr_field['type_args']['wpezfields'] ) ) {

                    $arr_ret = $this->ezLoader( $arr_field['type_args']['wpezfields'] );

                    //  if ($arr_field['type'] == 'collection') {
                    // we're going to "flaten out" the collections
                    $arr_pre = array_merge( $arr_pre, $arr_ret );
                    continue;
                    //}
                }

                $str_type = trim( str_replace( '-', '_', $arr_field['type'] ) );

                // TODO - use the current NS to sort out this NS
                $str_type_namespace = 'WPezSuite\WPezMeta' . '\Containers\\';
                if ( isset( $arr_field['type_namespace'] ) ) {
                    $str_type_namespace = trim( $arr_field['type_namespace'] ) . '\\';
                }
                $str_ele = $str_type_namespace . $str_type . '\\Class' . $str_type;

                // no class for the type then active = false
                if ( ! class_exists( $str_ele ) ) {
                    $arr_field['active'] = false;

                    die( $str_ele );
                    continue;
                }

                $arr_field['this_class']  = $str_ele;
                $obj_ele                  = new $str_ele();
                $arr_field['this_object'] = $obj_ele;

                $arr_field = array_replace_recursive( $this->elementDefaults(), $obj_ele->elementDefaults(), $arr_field );


                // now let's sort out the register meta stuff
                if ( $arr_field['register_meta'] === false ) {

                    // you're not going to registerMeta()...
                    $arr_field['register_meta']['active'] = false;
                    // ...but we still needs at least these for the save / get_value
                    $arr_field['register_meta']['meta_key']       = $arr_field['name'];
                    $arr_field['register_meta']['object_type']    = $this->_register_meta_object_type;
                    $arr_field['register_meta']['args']['single'] = $this->_register_meta_args_single;

                } else {

                    if ( $arr_field['register_meta'] === true ) {
                        $arr_field['register_meta'] = $this->registerMetaDefaults();
                    } elseif ( is_array( $arr_field['register_meta'] ) ) {
                        $arr_field['register_meta'] = array_merge( $this->registerMetaDefaults(), $arr_field['register_meta'] );
                    }

                    if ( $arr_field['register_meta']['args'] === true ) {
                        $arr_field['register_meta']['args'] = $this->registerMetaArgsDefaults();
                    } elseif ( is_array( $arr_field['register_meta']['args'] ) ) {
                        $arr_field['register_meta']['args'] = array_merge( $this->registerMetaArgsDefaults(), $arr_field['register_meta']['args'] );
                    } else {
                        // TODO - some catchall condition? e.g. === false - should be make/allow a decision?
                    }

                    if ( ! is_string( $arr_field['register_meta']['meta_key'] ) ) {
                        $arr_field['register_meta']['meta_key'] = $arr_field['name'];
                    }

                    if ( ! is_string( $arr_field['register_meta']['object_type'] ) ) {
                        $arr_field['register_meta']['object_type'] = $this->_register_meta_object_type;
                    }

                    if ( ! is_string( $arr_field['register_meta']['args']['description'] ) ) {
                        $arr_field['register_meta']['args']['description'] = $arr_field['label'];
                    }
                }

                // ----

                // is this type a composite?
                if ( isset( $arr_field['type_args']['wpezfields'] ) && is_array( $arr_field['type_args']['wpezfields'] ) ) {

                    $arr_field['type_args']['wpezfields'] = $this->ezLoader( $arr_field['type_args']['wpezfields'], $arr_field );

                }


                if ( ! is_array( $arr_field['active_blacklist'] ) ) {
                    $arr_field['active_blacklist'] = [];
                }

                $arr_field['type_args']['wrapper']['global_attrs'] = $this->wrapperGlobalAttrs( $arr_field );

                if ( $arr_field['active'] === true && is_array( $arr_parent ) && ! empty( $arr_parent ) ) {
                    // TODO - is this the correct / best esc_*?
                    $str_prefix        = esc_attr( $arr_parent['name'] );
                    $arr_field['name'] = $str_prefix . '_' . $arr_field['name'];
                    if ( isset( $arr_parent['register_meta']['meta_key'] ) ) {
                        $arr_field['register_meta']['meta_key'] = esc_attr( $arr_parent['register_meta']['meta_key'] ) . '_' . $arr_field['register_meta']['meta_key'];
                    } else {
                        $arr_field['register_meta']['meta_key'] = $str_prefix . '_' . $arr_field['register_meta']['meta_key'];
                    }
                }

                // TODO - check this
                if ( $arr_field['id'] === false || $arr_field['id'] === true || empty( esc_attr( $arr_field['id'] ) ) ) {
                    $arr_field['id'] = $arr_field['name'];
                }

                if ( is_array( $arr_field['label_global_attrs'] ) && ! isset( $arr_field['label_global_attrs']['for'] ) ) {

                    $arr_field['label_global_attrs'] = array_merge( $arr_field['label_global_attrs'], [ 'for' => $arr_field['id'] ] );

                } elseif ( ! is_array( $arr_field['label_global_attrs'] ) ) {
                    $arr_field['label_global_attrs'] = [
                        'for' => $arr_field['id']
                    ];
                }

            }

            $arr_pre[ $key ] = $arr_field;
        }

        return $arr_pre;
    }

    protected function elementDefaults() {

        $arr_defs = [
            'active'             => $this->_active,
            'active_blacklist'   => $this->_active_blacklist,
            'name'               => false, // REQUIRED
            'id'                 => false,
            'label'              => $this->_label,
            'label_global_attrs' => false,
            'desc'               => $this->_desc,
            'help'               => $this->_help,
            'type'               => false, // REQUIRED
            'type_namespace'     => $this->_namespace . '\WPezMeta\Containers\\',
            'type_args'          => false,
            'default_value'      => null,
            'global_attrs'       => [],
            'html_ir'            => [],
            // other gen markup ??
            'register_meta'      => true,

        ];

        return $arr_defs;
    }

    protected function thisSpecial() {

        $arr_spec = [
            // other stuff we're going to add as part of the processing
            'this_class'   => false,
            'this_object'  => false,
            'this_element' => $this->_this_element,
        ];

        return $arr_spec;
    }


    protected function registerMetaDefaults() {

        $arr_defs = [
            'active'      => $this->_register_meta_active,
            'object_type' => $this->_register_meta_object_type,
            'meta_key'    => false,
            'args'        => true
        ];

        return $arr_defs;
    }

    protected function registerMetaArgsDefaults() {

        $arr_defs = [
            'type'         => $this->_register_meta_args_type,
            'description'  => false,
            'single'       => $this->_register_meta_args_single,
            'show_in_rest' => $this->_register_meta_args_show_in_rest
        ];

        return $arr_defs;
    }

    /**
     * @param array $arr_field
     *
     * @return mixed|string
     */
    protected function wrapperDefaults( $arr_field = [] ) {

        // perhaps some overkill here. revist === TODO
        $arr_class   = [];
        $arr_class[] = esc_attr( $this->_wrapper_class_default );
        $arr_class[] = 'wpezmeta-field-' . esc_attr( $arr_field['type'] );

        //    $arr_class[] = esc_attr($this->_prefix) . 'wpezmeta_field_' . esc_attr($arr_field['type']);

        return implode( ' ', $arr_class );
    }


    protected function wrapperGlobalAttrs( $arr_field ) {

        $arr_wrapper_ga = [
            'id'    => esc_attr( $arr_field['name'] ) . '-wrapper',
            'class' => ''
        ];
        if ( isset( $arr_field['type_args']['wrapper']['global_attrs'] ) && is_array( $arr_field['type_args']['wrapper']['global_attrs'] ) ) {
            $arr_wrapper_ga = array_merge( $arr_wrapper_ga, $arr_field['type_args']['wrapper']['global_attrs'] );
        }
        $arr_wrapper_ga['class'] .= ' form-field' . ' ' . $this->wrapperDefaults( $arr_field );

        return $arr_wrapper_ga;
    }

}