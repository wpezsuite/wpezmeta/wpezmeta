<?php

namespace WPezSuite\WPezMeta\Services\Nonce;


// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassNonce {

    protected $_nonce_defaults = [
        'action'  => -1,
        'name'    => '_wpnonce',
        'referer' => true,
        'echo'    => false,
        // note: WP's default is true, but we're doing it "The ezWay" and will echo it ourselves
    ];
    protected $_nonce_active   = false;
    protected $_nonce_args     = [];

    public function __construct() {
    }

    /**
     * Not many reasons to change the defaults when you can load any values you
     * want, but WTHeck? :)
     *
     * @param array $arr_defs_merge
     */
    public function updateNonceDefaults( $arr = [] ) {

        if ( is_array( $arr ) ) {
            $this->_nonce_defaults = array_merge( $this->_nonce_defaults, $arr );
        }
    }


    public function ezLoader( $arr_nonce ) {

        $this->_nonce_args = $this->_nonce_defaults;
        if ( is_array( $arr_nonce ) ) {
            $this->_nonce_args = array_merge( $this->_nonce_defaults, $arr_nonce );
        }

        $this->_nonce_active = true;

        return true;
    }

    /**
     * @param array $arr_nonce
     */
    public function wpNonceField() {

        if ( $this->_nonce_active !== true ) {
            $this->_nonce_args = $this->_nonce_defaults;
        }

        $str_ret = wp_nonce_field(
            $this->_nonce_args['action'],
            $this->_nonce_args['name'],
            $this->_nonce_args['referer'],
            $this->_nonce_args['echo']
        );

        return $str_ret;
    }

}