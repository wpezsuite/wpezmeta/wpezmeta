<?php

namespace WPezSuite\WPezMeta\Services\EnqueueStyles;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassEnqueueStyles {


    protected $_style_defaults = [
        'active' => true,
        'handle' => false,
        'src'    => false,
        'deps'   => [],
        'ver'    => false,
        'media'  => 'all'
    ];


    protected $_arr_assets = [];


    /**
     * @param $arr
     */
    public function updateStyleDefaults( $arr ) {

        if ( is_array( $arr ) ) {
            $this->_style_defaults = array_merge( $this->_style_defaults, $arr );
        }
    }


    /**
     * @param array $arr_wpezfields
     */
    public function ezLoader ( $arr_wpezfields = [] ) {

        if ( is_array( $arr_wpezfields ) ) {

            $this->preprocessor( $arr_wpezfields );
        }
    }


    public function wpRegisterStyles () {

        $arr_assets = [];
        foreach ( $this->_arr_assets as $arr ) {

            $arr = array_merge( $this->_style_defaults, $arr );

            if ( $arr['active'] !== false ) {

                $bool = wp_register_style(
                    $arr['handle'],
                    $arr['src'],
                    $arr['deps'],
                    $arr['ver'],
                    $arr['media']
                );
                if ( $bool ) {
                    $arr_assets[] = $arr;
                }
            }
        }
        $this->_arr_assets = $arr_assets;
    }


    public function wpEnqueueStyles () {

        foreach ( $this->_arr_assets as $arr ) {

            $arr = array_merge( $this->style_defaults(), $arr );

            if ( $arr['active'] !== false ) {
                wp_enqueue_style( $arr['handle'] );
            }
        }
    }


    /**
     * @param array $arr_args
     *
     * @return string
     */
    protected function preprocessor ( $arr_wpezfields = [] ) {

        $arr_assets = [];
        foreach ( $arr_wpezfields as $key => $arr_field ) {

            if ( $arr_field['active'] !== false ) {

                if ( $arr_field['type'] == 'collection' ) {

                    $arr_field['active'] = false;
                    continue;
                }
                // now get an special reg_meta
                $arr_temp = $arr_field['this_object']->styles( $arr_field );
                // merge and keep going
                $arr_assets = array_merge( $arr_assets, $arr_temp );

                // is this type made up of other elements?
                if ( isset( $arr_field['type_args']['wpezfields'] ) && is_array( $arr_field['type_args']['wpezfields'] ) ) {
                    $arr_temp = $this->preprocessor( $arr_field['type_args']['wpezfields'] );
                    if ( is_array( $arr_temp ) ) {
                        $arr_assets = array_merge( $arr_assets, $arr_temp );
                    }
                }
            }
        }

        $this->_arr_assets = $arr_assets;
    }

}