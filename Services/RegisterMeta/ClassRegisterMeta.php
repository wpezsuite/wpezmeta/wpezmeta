<?php

namespace WPezSuite\WPezMeta\Services\RegisterMeta;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassRegisterMeta {

    // register meta (as seen mentioned here:
    // http://themehybrid.com/weblog/introduction-to-wordpress-term-meta )
    // https://make.wordpress.org/core/2016/07/20/additional-register_meta-changes-in-4-6/


    /**
     * @param array  $arr_wpezfields
     * @param string $str_prefix
     */
    public function ezLoader( $arr_wpezfields = [], $wp_object = false ) {

        if ( ! is_array( $arr_wpezfields ) || empty( $arr_wpezfields ) || $wp_object === false ) {
            return [ false ];
        }
        $arr_reg_meta = $this->preprocessor( $arr_wpezfields );
        return $this->registerMeta( $arr_reg_meta, $wp_object );
    }


    protected function registerMeta( $arr_reg_meta, $wp_object = false ) {

        $arr_ret = [];
        foreach ( $arr_reg_meta as $key => $arr ) {

            // the WP register_meta function returns true or false, so we might as well capture that
            // and return it for debugging purposes
            $arr_ret[ $key ] = register_meta( $arr['meta_key'], $wp_object, $arr['args'] );
        }

        return $arr_ret;
    }


    /**
     * @param array $arr_args
     *
     * @return string
     */
    protected function preprocessor( $arr_wpezfields = [] ) {

        $arr_reg_meta = [];
        foreach ( $arr_wpezfields as $key => $arr_field ) {

            if ( $arr_field['active'] !== false ) {

                if ( $arr_field['type'] == 'collection' ) {

                    $arr_field['active'] = false;
                    continue;

                } else {

                    if ( $arr_field['register_meta']['active'] !== false ) {

                        // get the reg meta from the field definition (array)
                        $arr_reg_meta[ $key ] = $arr_field['register_meta'];
                        // now get any special reg_meta
                        $arr_rm = $arr_field['this_object']->registerMeta( $arr_field );
                        // merge and keep going
                        if ( is_array( $arr_rm ) ) {
                            $arr_reg_meta = array_merge( $arr_reg_meta, $arr_rm );
                        }
                    }
                    // is this type made up of other elements?
                    if ( isset( $arr_field['type_args']['wpezfields'] ) && is_array( $arr_field['type_args']['wpezfields'] ) ) {

                        $arr_rm = $this->preprocessor( $arr_field['type_args']['wpezfields'] );
                        if ( is_array( $arr_rm ) ) {
                            $arr_reg_meta = array_merge( $arr_reg_meta, $arr_rm );
                        }

                    }
                }
            }
        }

        return $arr_reg_meta;
    }
}