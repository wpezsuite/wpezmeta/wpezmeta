<?php

namespace WPezSuite\WPezMeta\Services\Wrapper;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassWrapper {

    use \WPezSuite\WPezCore\Traits\MarkupGeneration\TraitMarkupGeneration;

    protected $_flag = false;

    protected $_wpezfields;
    protected $_wp_object;
    protected $_wp_object_id;
    protected $_use_default_value;
    protected $_wrapper = 'add_form_fields';
    protected $_echo    = true;

    protected $_Xwrapper_default        = 'wpezmeta-field-wrapper';
    protected $_wrapper_label_help      = 'wpezmeta-label-help-wrapper';
    protected $_add_form_fields_default = 'c1';

    protected $_array  = [];
    protected $_string = '';


    public function __construct() {
    }


    public function setWrapper( $str ) {

        $this->_wrapper = $str;
    }


    public function setEcho( $bool ) {

        $this->_echo = (bool)$bool;
    }


    public function setWrapperDefault( $str ) {

        $this->_wrapper_default = $str;
    }


    public function setWrapperLabelHelp( $str ) {

        $this->_wrapper_label_help = $str;
    }

    public function setAddFormFieldsDefault( $str ) {

        $method = 'add_form_fields_' . ( trim( strtolower( $str ) ) );
        if ( method_exists( $this, $method ) ) {
            $this->_add_form_fields_default = ( trim( strtolower( $str ) ) );
        }
    }


    public function getWPezFields() {

        return $this->_wpezfields;

    }

    public function getArray() {

        return $this->_array;


    }

    public function getString( $glue = "\n" ) {

        return implode( $glue, $this->_array );

    }


    /**
     * @param array $arr_args
     *
     * @return string
     */
    public function ezLoader( $arr_args = [] ) {

        if ( ! is_array( $arr_args ) ) {
            return false;
        }

        $arr_args = array_merge( $this->argsDefaults(), $arr_args );

        if ( ! isset( $arr_args['wpezfields'],
            $arr_args['wp_object'],
            $arr_args['wp_object_id'],
            $arr_args['use_default_value'] )
        ) {
            return false;
        }

        if ( ! method_exists( $this, $arr_args['wrapper'] ) ) {
            return false;
        }

        if ( ! is_array( $arr_args['wpezfields'] ) ) {
            return false;
        }

        // all is well
        $this->_flag = true;

        $this->_wpezfields        = $arr_args['wpezfields'];
        $this->_wp_object         = $arr_args['wp_object'];
        $this->_wp_object_id      = $arr_args['wp_object_id'];
        $this->_use_default_value = $arr_args['use_default_value'];
        $this->_wrapper           = $arr_args['wrapper'];
        $this->_echo              = (boolean)$arr_args['echo'];

    }

    protected function argsDefaults() {

        $arr = [
            'wrapper' => $this->_wrapper,
            'echo'    => $this->_echo,
        ];

        return $arr;
    }


    public function goWrapper() {

        if ( $this->_flag === true ) {

            $arr_wpezfields = $this->_wpezfields;
            $arr            = [];
            foreach ( $this->_wpezfields as $key => $arr_field ) {

                if ( $arr_field['active'] !== false ) {

                    //	$new_ele              = new $str_ele();
                    if ( ! isset( $arr_field['this_object'] ) || $arr_field['this_object'] === false ) {
                        continue;
                    }
                    $method = $this->_wrapper;

                    $temp                                                = $this->$method( $arr_field );
                    $arr_wpezfields[ $key ]['this_element']['assembled'] = $temp;
                    $arr[ $arr_field['name'] ]                           = $temp;

                }
            }

            $this->_wpezfields = $arr_wpezfields;
            $this->_array      = $arr;
        }
    }


    protected function prep( $arr_field = [] ) {

        $this_object = $arr_field['this_object'];
        $arr_field   = $this_object->prep( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value );

        return $arr_field;

    }

    protected function label( $arr_field = [] ) {

        $str_ret     = '';
        $this_object = $arr_field['this_object'];
        $str_ret     = $this_object->label( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value, $this->_echo );

        return $str_ret;
    }

    protected function help( $arr_field = [] ) {

        $str_ret     = '';
        $this_object = $arr_field['this_object'];
        $str_ret     = $this_object->help( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value, $this->_echo );

        return $str_ret;
    }

    protected function desc( $arr_field = [] ) {

        $str_ret     = '';
        $this_object = $arr_field['this_object'];
        $str_ret     = $this_object->desc( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value, $this->_echo );

        return $str_ret;
    }

    protected function control( $arr_field = [] ) {

        $str_ret     = '';
        $this_object = $arr_field['this_object'];
        $str_ret     = $this_object->control( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value, $this->_echo );

        return $str_ret;
    }

    protected function extra( $arr_field = [] ) {

        $str_ret     = '';
        $this_object = $arr_field['this_object'];
        $str_ret     = $this_object->extra( $arr_field, $this->_wp_object, $this->_wp_object_id, $this->_use_default_value, $this->_echo );

        return $str_ret;
    }

    /**
     * @param array $arr_field
     *
     * @return string
     */
    protected function add_form_fields( $arr_field = [] ) {

        return $this->add_form_fields_c1( $arr_field );

    }

    /**
     * @param array $arr_field
     *
     * @return string
     */
    protected function add_form_fields_ver( $arr_field = [] ) {

        $method = 'add_form_fields_' . $this->_add_form_fields_default;
        if ( isset( $arr_field['type_args']['wrapper']['ver'] ) ) {
            $temp = 'add_form_fields_' . trim( strtolower( $arr_field['type_args']['wrapper']['ver'] ) );
            if ( method_exists( $this, $temp ) ) {
                $method = 'add_form_fields_' . trim( strtolower( $arr_field['type_args']['wrapper']['ver'] ) );
            }

        }

        return $this->$method( $arr_field );

    }

    protected function add_form_fields_c1( $arr_field = [] ) {

        $arr_field = $this->prep( $arr_field );

        $str_ret = '<div ' . $this->global_attrs( $arr_field['type_args']['wrapper']['global_attrs'] ) . '>';
        $str_ret .= '<span class="' . esc_attr( $this->_wrapper_label_help ) . '">';
        if ( $this->_echo !== false ) {
            echo $str_ret;
        }

        $str_ret2 = $this->label( $arr_field );
        $str_ret2 .= $this->help( $arr_field );

        $str_ret3 = '</span>';
        if ( $this->_echo !== false ) {
            echo $str_ret3;
        }

        $str_ret4 = $this->control( $arr_field );
        $str_ret4 .= $this->desc( $arr_field );
        $str_ret4 .= $this->extra( $arr_field );

        $str_ret5 = '<hr>';
        $str_ret5 .= '</div>';

        if ( $this->_echo !== false ) {
            echo $str_ret5;
        }

        return $str_ret . $str_ret2 . $str_ret3 . $str_ret4 . $str_ret5;
    }

    protected function add_form_fields_c2( $arr_field = [] ) {

        $str_ret = '<div ' . $this->global_attrs( $arr_field['type_args']['wrapper']['global_attrs'] ) . '>';
        $str_ret .= '<table class="form-table">';
        $str_ret .= '<tr>';
        $str_ret .= '<th scope=""row">';
        $str_ret .= '<span class="' . esc_attr( $this->_wrapper_label_help ) . '">';
        if ( $this->_echo !== false ) {
            echo $str_ret;
        }

        $str_ret2 = $this->label( $arr_field );
        $str_ret2 .= $this->help( $arr_field );

        $str_ret3 = '</span>';
        $str_ret3 .= '</th>';
        $str_ret3 .= '<td>';
        $str_ret3 .= '<div class="wpezmeta-add-form-fields wpezmeta-c2-2">';
        if ( $this->_echo !== false ) {
            echo $str_ret3;
        }

        $str_ret4 = $this->control( $arr_field );
        $str_ret4 .= $this->desc( $arr_field );
        $str_ret4 .= $this->extra( $arr_field );

        $str_ret5 = '</div>';
        $str_ret5 .= '</td></tr>';
        $str_ret5 .= '</table>';
        $str_ret5 .= '<hr>';
        $str_ret5 .= '</div>';

        if ( $this->_echo !== false ) {
            echo $str_ret5;
        }

        return $str_ret . $str_ret2 . $str_ret3 . $str_ret4 . $str_ret5;
    }


    protected function edit_form_fields( $arr_field = [] ) {

        $str_ret = '<tr ' . $this->global_attrs( $arr_field['type_args']['wrapper']['global_attrs'] ) . '>';
        $str_ret .= '<th scope="row">';
        $str_ret .= '<span class="' . esc_attr( $this->_wrapper_label_help ) . '">';
        if ( $this->_echo !== false ) {
            echo $str_ret;
        }

        $str_ret2 = $this->label( $arr_field );
        $str_ret2 .= $this->help( $arr_field );

        $str_ret3 = '</span>';
        $str_ret3 .= '</th>';
        $str_ret3 .= '<td>';
        if ( $this->_echo !== false ) {
            echo $str_ret3;
        }

        $str_ret4 = $this->element( $arr_field );
        $str_ret4 .= $this->desc( $arr_field );
        $str_ret4 .= $this->extra( $arr_field );

        $str_ret5 = '</td>';
        $str_ret5 .= '</tr>';
        if ( $this->_echo !== false ) {
            echo $str_ret5;
        }

        return $str_ret . $str_ret2 . $str_ret3 . $str_ret4 . $str_ret5;

    }

}